# **VERS v20.7**

## _Compact Edition_


# Introduction

VERS is a table-top roleplaying game that has OSR (Old School Renaissance) bones with a lot of modern elements to round it off. It is not, like many OSR games, a retread of an older existing game, but instead a new game that adheres to a lot of what makes OSR work. This version of the game is the bare bones, system resource document style version of the game. It is 100% the exact same game as the full version, but without all of the explanations and examples. In other words, it assumes that you have played and run a roleplaying game before.

VERS is free-form, in that it does not have classes or levels. Instead, it allows you to build whatever you want, in whatever genre you want. Yes, there are a lot of other games out there like this, but the vast majority of them are huge, bullet-proof tomes, with reams of rules for every situation. VERS is different. It started with the idea of taking those gameplay ideas and stripping them down to the simplest form possible. Then those building blocks were unified to use as few different mechanics as possible and as few dice rolls as possible.

The result, I hope, is a game that is easy to learn, easy to play, and easy to customize to whatever setting or genre you can imagine. And because it is completely open source, you can feel free to modify it, reuse it, and share it with all of your players without worry. And yes, print shop guy or gal, that means they have permission to print it if they want. Just don’t try to sell it, please, although you can definitely sell anything you derive from it, like settings or supplements. Just link back here, and drop my name in the credits. You don’t even have to ask.

So, grab some dice, some friends, and some paper, and see what you can make. And if you break it, let me know, so I can patch it up. It is completely impossible to guarantee no bugs in a game this wide open, but I have done what I can. Enjoy.

Jason Murray
Author


# Basics

Nearly all rolls are made by the player. Rolls are either Tests, Contests, or Reactions. Tests are rolls with no opposition from other characters, and often have penalties set by the environment or circumstances. Contests, on the other hand, are directly opposed by at least one other character and have penalties set by the Skills of that opposing NPC. However, they can also have additional modifiers from the environment and circumstances as well. Reactions are responses to actions, such as dodging an attack or avoiding a trap, and can otherwise be Tests or Contests depending on the situation.

Sometimes the GM will wish to keep results of a roll secret and will make them for the player, although this should be kept to a minimum.

Rolls are always 3d6 plus any relevant Skill and/or miscellaneous bonuses, and measured against a Target Number (TN) set by the character's Attributes. The difference between the TN and the result is important, and is referred to as Degrees of Success (or Degrees of Failure). These can be "spent" on Stunts, which are similar to criticals in other systems, and add effects to the success or failure.

The effects of a roll are determined by the Effect Value, or EV, compared to the target’s RV or Resistance Value. The effect is set by the action or ability being performed. For an attack the effect is damage, and the EV is the total amount of damage that the target could take. For a psychic telekinesis effect, the EV may be the effective Strength that the character can use to lift things. For an invisibility spell, the EV may be a bonus to Stealth rolls. The RV is typically armor or magical protection of some kind, although in certain situations the target’s Attributes may contribute.

On their Turn, every character can make 2 Standard Actions, which can consist of moving, attacking, or activating an ability. There are also free actions, which are defined by the GM, but should be simple like saying a word ("Halt!") or pressing a button. Characters can take the same action twice in a row, such as making a double move, two attacks, or activating two abilities.

During an episode, a player can request a "Flashback," which is a short scene in which they prepare for whatever problem they are currently encountering. The main rule here is that the flashback cannot change known facts (If the officer is standing in front of you, you cannot flashback and have him assassinated, for example). GM has final say on when and if flashbacks are available and what the final outcome is. Flashbacks are roleplayed scenes, however, and not just "I Win" buttons, and the actions taken during them must make sense within the scope of the character, the setting, and the situation.

Finally, a reminder. Dice are important tools in roleplaying games, however, over reliance on them slows the game, bores the players, and sucks the energy out of the story. Both player and GM should remember this golden rule: **_Only roll dice if the player can fail, and the failure result is interesting from a story perspective_**.

Yes, you may fail to tie your shoe, but no one cares. There is no drama there. Failing to disarm that bomb, on the other hand? Yea, there are a lot of people who are going to care about that. And a character who couldn’t fail the roll? Also, no drama or interest there. Just let them succeed and carry on with the story. Every die roll should count, and an important dramatic moment should rest on that result. Otherwise just narrate it and move the game forward.


# Making a Character

The first step in making a character is the character concept. This is in the form of a sentence up to a short paragraph, and it spells out in normal language what the character is about. This concept is referred back to numerous times throughout the rest of the creation steps.


## Momentum

Every character has Momentum, which is that special something that separates heroes from regular characters. Momentum can be spent to gain special benefits. These are gaining a +3 bonus on a roll, gaining an extra action on your turn, or gaining a recovery action regardless of when the next one is due. The GM may define other uses for momentum as well.

Additionally, players can tie their use of Momentum to their Talent, Flaw, Anchor, or Motivation. Using it in this way (as part of roleplaying their character) upgrades the benefit, making the roll twice and taking the better result, allowing the player to take an additional full turn immediately after this one, or automatically removing the highest level condition on a single tracker. Only one point of momentum can be spent per turn, and it only applies to the action it was invoked for, and only for the character invoking it.

Characters start every episode with 1 Momentum, and can gain a maximum number of momentum points equal to their Power Level (minimum 1). Momentum is gained by the player choosing to "take a complication," or to purposefully use their Talent, Flaw, Anchor, or Motivation to negatively impact themselves and the party. NPCs should never have access to momentum, even heroic ones.


## Character Profile

The following five sections reveal the roleplay-centric parts of a character, essentially tools to enhance a player’s concept and use that information to inform their play. These act as a guide to those completely new to roleplaying, while the interactions with Momentum make them useful incentives for players of all types to be more than just numbers on a page.


### Talent

Everyone is good at something. A character's Talent is the one thing they are better than everyone else at. This does not have to be a Skill, like Pilot, but can be something more nebulous like "strategy games" or "extreme sports." Each character's Talent is unique, and no other player can have one that is the same or even sufficiently similar, with the GM as the final arbiter.

When the character’s talent could be reasonably determined to come into play, the character can get a benefit. This should typically not be a bonus on the roll (although it can be) and should instead be something like extra information or roleplay opportunities they otherwise might not have gotten (job opportunities, admirers, etc). It can also lead to hubris and failure due to overconfidence.


### Anchor

A person, place, or thing that holds emotional value for the character. This is the thing the character is fighting for, and pushes them to do great things. This can literally be anything (GM has final say), and does not have to be physically present to be used. The classic version of this is the picture taped to the fighter pilot’s cockpit.

Anchors can inspire heroic actions, fighting for loved ones or to protect important places, but they can also inspire acts of great evil as that desire drives a character to whatever means necessary to reach their goal.


### Motivation

An anchor is a person, place, or thing that drives a character, but a Motivation is an ideal or circumstance.  Again, this could be anything, like Altruism, Purity, Greed, or a circumstance like War, Famine, or Plague.

The thing about a motivation is that it can inspire heroic action, especially the more “positive” motivations like Altruism, but it can also inspire zealotry, intolerance, and other negatives if others do not conform to your views. Meanwhile, the “negative” motivations, like Greed can still cause positive outcomes in the right situations.

In most settings and genres a character will only have 1 motivation, however in some cases the GM may allow or even require more.


### Flaw

Every character has something negative in their life, something that gives them problems or weighs them down. This could be guilt or other mental issues, a physical handicap, or even something social like a secret identity or a family they have to protect.

Regardless of what the flaw may be, characters can either give in to their flaw, allowing it to bring them down and defeat them or they can fight against them, using them as crucibles that forge them into greater versions of themselves. In most settings or genres characters will only have 1 flaw, but can have more at GM discretion.


### Relationships

Every character should have some type of story connection to at least half of the other characters in the troupe. These connections could be familial, friendships, or even a rivalry. Just remember, relationships between player characters have to be agreed on by both players. This connects the troupe together into something more cohesive, and gives them a reason to work together while also still allowing some conflicts to occur just as they do in real relationships.

In addition to the relationships with other player characters, each character should also define relationships to at least two NPCs. These can be any of the above relationships, but they could also be mentors, dependants, or even enemies. These NPC relationships give the GM ways to connect the character to the world and the events.


## Mechanical Aspects

The following sections go into more details on the more mechanical sections of the character, and how they interact with the physics of the world around them. These are the parts that most roleplaying games focus on, and they are integral to VERS as well.


### Power Source

A character's power source is essentially a one or two word descriptor of what powers their abilities. Typical examples are magic, technology, psychic, alien biology, and martial arts. In a realistic war game or any other genre that does not use abilities, this can be skipped, or the training Power Source can be used.

The power source descriptor allows certain abilities to target only certain types of characters, such as "protection against evil" spell preventing characters with the “Evil” power source, or a "signal jammer" device that prevents an AI character from moving or acting.


### Power Level

A character's power level is a measure of how powerful the character is. It gives a character their starting Build Point totals and it also functions as a loose guideline for balancing character creation and character advancement by placing a general limit on the character’s stats. No character should have an Attribute, Skill, or Ability ranked above this value, although exceptions can be made by the GM.

| Power Level and Character Creation                                    |
|:--:|:---------:|:-----:|:-------:|:----------------------------------:|
| PL | Attribute | Skill | General |              Examples              |
|  0 |     5     |   5   |    0    |             Typical NPC            |
|  1 |     10    |   7   |    0    |    Teenage PCs, Adventuring NPCs   |
|  2 |     15    |   10  |    5    |              Novice PC             |
|  3 |     20    |   10  |    7    |     Standard PC, Important NPC     |
|  4 |     25    |   13  |    10   |     Veteran PC, Teen Superhero     |
|  5 |     30    |   15  |    15   | Low Level Superhero, Legendary NPC |
|  6 |     35    |   20  |    25   |        Superhero, Mythic NPC       |
|  7 |     40    |   25  |    35   |          Veteran Superhero         |
|  8 |     45    |   30  |    50   |                                    |
|  9 |     50    |   35  |    65   |                                    |
| 10 |     55    |   40  |    80   |         Practically a deity        |

Most games will start out at PL 3, although low fantasy or horror games may start out at PL 2, high fantasy games often start at PL 4, and superhero games range from PL 4-7. In games or genres in which players often have more abilities (high fantasy or superheroes) the GM may double the general BP total in the chart.

Finally, Build Points are different from Character Points, both in that they are spent differently and that BP is only available during character creation, while CP is only available after. Attribute and Skill BP is spent 1 for 1, or in other words, 1 BP gets 1 rank, while CP has a diminishing value for both, costing progressively more for the next rank. General BP spends the same way as CP, and can be converted to CP if it is not all spent at character creation.


### Attributes

Attributes are the character's inborn talents, and reflect what they are good at before education, training, or practice (those are skills). Attributes are relative, meaning that each rank is just a representation of difference from the average, not a hard, absolute value with a concrete meaning. The average for all attributes is 0, and characters can have attributes above and below 0, meaning they are better or worse than average.

Characters can “sell back” attributes at character creation, giving their characters a negative rank and getting back build points. No player character should have more than a -2 in any attribute, although the decision is ultimately with the GM.

When progressing a character after character creation, each new rank costs Character Points equal to the new rank times 3. In other words, to go from Strength 2 to Strength 3 costs 9 CP. To go up to 4 afterwards will cost another 12 CP. Note, these increases must progress through each step. The character cannot go from 2 straight to 4 and only pay 12 CP.

Attributes form the Target Number for any skill roll a character needs to make. This TN is equal to twelve minus the attribute rank, (negative attributes raise the TN instead) and the roll succeeds if it rolls equal to or greater than that number.

Attributes are divided into a 3x3 matrix based on what function they play and what part of the character they govern. The columns are Mental, Physical and Social, and the rows are Power, Finesse, and Resistance. The nine Attributes basically describe the different conjunctions of these rows and columns.


#### Mental

*   **Logic:** The Mental Power attribute, Logic deals with raw computational intelligence, memorization of facts, and understanding abstract ideas.
*   **Intuition:** The Mental Finesse attribute, Intuition deals with creativity, emotional intelligence, and pattern recognition.
*   **Resolve:** The Mental Resistance attribute, Resolve deals with a character's willpower, their ability to focus, and their ability to withstand temptations.


#### Physical

*   **Strength:** The Physical Power attribute, Strength measures a character's ability to lift, pull, and push objects. The only attribute with a real world measurement possible, although GMs are encouraged to keep it loose.
*   **Agility:** The Physical Finesse attribute, Agility measures both a character's fine and gross motor control, as well as their reflexes.
*   **Stamina:** The Physical Resistance attribute, Stamina measures a character's ability to shrug off pain, fight off disease, and endure prolonged work or exercise.


#### Social

*   **Presence:** The Social Power attribute, Presence is a measure of the character's ability to draw attention to themselves and bend the wills of others.
*   **Charisma:** The Social Finesse attribute, Charisma is a measure of the character's ability to subtly manipulate and attract others.
*   **Composure:** The Social Resistance attribute, Composure is the character's ability to retain their wits in difficult situations, as well as resist the manipulation and intimidation of others.


#### Figured Attributes

*   **Discipline:** The combination of Composure and Resolve, Discipline is a measure of one's ability to withstand situations and events that frighten, demoralize, distract, or otherwise affect the character's mind and emotions. A character treats the higher of the two values as the TN and uses the lower as the bonus on the roll.
*   **Dodge:** The combination of Intuition and Agility, Dodge is a measure of a character’s reflexes, allowing them to evade physical attacks, avoid traps and environmental dangers. A character treats the higher of the two values as the TN and uses the lower as the bonus on the roll.


### Skills

If attributes are a character’s inborn talents, then skills are the things that a character has learned over the course of their lives. Every skill is (loosely) tied to one of the nine attributes, but this connection is not set in stone. What this means is that, while Close Combat is normally based on Agility, in this case, based on the events in the game, the GM could decide that for this one attack, Logic should be the related attribute. This won’t happen often, but this is the GM’s decision to make. Remember, attributes determine the Target Number of a roll, while the skill provides a bonus to the roll.

When advancing a character with Character Points, skills cost 1 CP per new rank. In other words, going from rank 2 to rank 3 costs 3 CP, while going from rank 3 to rank 4 costs 4 CP, etc. Like Attributes, Skills must be raised one step at a time. A character cannot go from rank 2 to rank 4 for 4 CP. It would cost 7 Character Points, 3 CP for the 3rd rank and 4 CP for the 4th.


#### Background

In addition to the normal skills a character has access to, a character also has a Background which gives them access to specialized knowledge and skills related to their careers, training, hobbies, and general life. For example, a pirate character might apply their Background bonus to sail a ship, read a map, or maybe know the laws of a port city. They probably wouldn’t get it on a roll regarding the obscure points of a religious doctrine or if they try to forge a blade. Of course, if the character’s concept that they were raised in an orphanage ran by a temple dedicated to the smith god, all bets are off.

GMs are encouraged to keep this loose and nebulous, but those that like more consistency can instead have players make a list of five or six “Special Skills” (see below) to serve as their Background. Backgrounds are treated as skills of rank 2 (they give a +2 bonus, in other words).


#### Synergy

Sometimes one skill may influence the use of another skill. This may be one character helping another, or maybe one of the same character’s skills coming into play. A good example of this might be gaining a bonus on an Influence roll due to the character’s knowledge of etiquette (either represented by a Noble background, or perhaps ranks in the special skill Expertise (Etiquette). Regardless, if the GM decides that a synergy bonus is warranted, the primary person making the roll gets a +1 bonus for every 2 ranks of the related skill, rounded down. Multiple synergies can come into play on the same roll, however this is solely at the GM’s discretion.


#### Basic Skills

*   **Animal Handling:** Training and manipulating animals, including calming, riding, and otherwise working with them. This does not, however, confer any supernatural ability to communicate. Typically based on Presence.
*   **Armor Use:** Training with the wearing and use of armor, giving the character an understanding of how to work with the armor instead of against it. Can be used instead of Dodge to avoid damage. Successful use allows a character to ignore all damage, while failure means the character does not get the damage reduction from their armor. A character using this skill is always hit, only the outcome changes. Typically based on Intuition.
*   **Athletics:** Training with using the body, including running, jumping, climbing, swimming, lifting, pushing, pulling, and nearly any other similar situation that is not otherwise covered by another skill. Typically based on Strength, Agility, or Stamina.
*   **Close Combat:** Training with fighting with the body (as in punches, kicks, elbows, knees, head, etc) and non-ranged weapons (such as swords, clubs, spears, baseball bats, crowbars, laser swords, etc). In some settings the GM may choose to break this up into smaller skills. A character can also use this skill in place of Dodge to protect themselves, with successful use parrying the blow with the weapon in question (including bare hands), and failure meaning the character is hit. Typically based on Agility.
*   **Contortion:** Training with bending and moving the body in atypical ways, allowing access to small spaces or escaping from bonds. Typically based on Agility.
*   **Deception:** Training in fooling a target, either by outright lies and fabrications or half-truths and bluffs. This deception can be both verbal and/or in body language, and also covers disguises. Typically based on Intuition.
*   **First Aid:** Training in basic medical procedures, such as field dressing a wound, splinting and binding a broken limb, CPR, and other similar techniques, depending on the genre and setting. This is not analogous to the actual medical knowledge needed to practice medicine, as that would fall under Background or a Special Skill. Typically based on Logic.
*   **Influence:** Training in convincing others, whether it be by honeyed words or unsubtle threats. Covers intimidation, diplomacy, persuasion, seduction, or any other methods of coercion not covered by another skill. In some settings and genres the GM may decide to break this down into smaller skills. Typically based on Charisma or Presence. 
*   **Insight:** Training in recognizing patterns, and/or logically connecting events. Also useful for seeing through Deceptions. Successful rolls can provide players with hints. This skill mostly exists to allow players to play detective characters or other similar archetypes without actually being personally good at these types of activities. Typically based on Logic or Intuition.
*   **Investigation:** Training in seeking information. This can be research, questioning witnesses or other persons of interest, or searching for physical evidence at a crime scene or the like. Typically based on Intuition.
*   **Linguistics:** Training in communication, specifically languages, although this also includes sign languages, dialects, creoles, and pidgins. This can help a character decipher a script, understand a spoken language based on cognates, or emulate dialects to pass themselves off as a native of a different place or being of a different social strata. Typically based on Logic.
*   **Meditation:** Training in focusing concentration, often with the added benefit of calming the mind and soothing the emotions. This can be traditional sitting meditation, moving meditation like yoga, or even prayer. Can be used like First Aid for Mental and Social damage. Typically based on Resolve.
*   **Perception:** Training in using a character’s senses: sight, hearing, and smell being the most common, but could even be used for taste, touch, hot, cold, pressure, or any other information that the body can obtain about the outside world. Typically based on Intuition.
*   **Ranged Combat:** Training in attacking at range, be it thrown weapons, archery, or firearms. In some settings GMs may break this down into smaller sub-skills. One important note, muscle powered ranged weapons (throwing axes, bows, etc) get a bonus to their damage based on the character’s Strength, however they can be Dodged (and parried and deflected with armor use). Mechanical ranged weapons, like crossbows, guns, and the like do not get a bonus from strength, however they also travel too fast to be Dodged. For more information, see the Gameplay section below. Typically based on Agility.
*   **Security:** Training in opening locks, bypassing alarms, and hacking firewalls, this skill can also be used to enable better security. Some settings may warrant breaking this up into multiple skills, at the GM’s discretion. Typically based on Logic.
*   **Stealth:** Training in hiding and remaining undetected, either while still or in motion. Typically based on Intuition.
*   **Survival:** Training in living off the land and navigating the wilderness. Typically based on Intuition.
*   **Teamwork:** Training in working with others, this skill allows multiple characters to synchronize their efforts to maximize their effectiveness. All characters taking part in the team-up will roll Teamwork, and everyone involved gets a bonus to their next action equal to the Degrees of Success on the worst roll (including giving all parties a penalty for a failed roll). Typically based on Intuition.
*   **Trading:** Training in haggling, appraising, and general economic concepts. Successes can give discounts while shopping, depending on the method of Gear acquisition being used (see the Gear section below for more information). Typically based on Intuition.


#### Special Skills

These skills are generally more often used as part of a Background, however, players wanting to specialize, have a larger bonus than +2, or just especially want to emphasize this aspect of their character may decide to take ranks in these skills directly.

*   **Art:** Training in the creation of works of physical art, such as painting, drawing, sculpture, and even poetry and literature. Degrees of Success increase the “quality” of a piece (whatever that really means), increasing the amount it can be sold for, etc. Typically based on Intuition.
*   **Engineering:** Training in the design, creation, and repair of functional technology, objects, and buildings. The output can also be artistic, but this skill is primarily focused on function. Typically based on Logic.
*   **Expertise:** Training in academic fields of study, such as philosophy, religion, mathematics, science, history, and many others. This also covers non-academic interests in which there also exists a large body of knowledge, such as comic book history, movie trivia, video game genres, or cosmetic application techniques. It is important to note, however, that Expertise is theory and abstract knowledge, not practical application of that knowledge, which falls into the other special skills. Typically based on Logic.
*   **Performance:** Training in the creation of works of temporal art, such as acting, music, or dance. The major difference between Art and Performance is the longevity of the actual creation. Writing a play is Art, performing the play is Performance. Typically based on Intuition.
*   **Pilot:** Training in the operation and guidance of a vehicle, especially under duress. In some genres this could also include animals such as horses or camels used for locomotion, especially if the character is able to ride but is not otherwise good with animals, like with Animal Handling. Typically based on Intuition.
*   **Profession:** Training in the day-to-day workings of a job or occupation, such as tavern keeper, janitor, or professor, especially those which would not fall under Art, Engineering, Performance, or Pilot. Typically based on Logic or Intuition.


### Abilities

Abilities are, in essence, everything else a character can do. Psychic abilities, punching through walls, casting spells, or anything else that makes sense in your game. Like everything else in VERS, abilities are based on ranks, and have a few guiding concepts. Ultimately, however, abilities are very freeform and individual.

Abilities are used in the same way that other actions are, making an activation roll, which is usually also a targeting roll (meaning that the roll may take a penalty based on Dodge, Discipline, or some other reaction skills). There are multiple ways to do this, based on the genre and setting, but GMs should decide on which method they are using for their game and stick with it across all abilities in that game.

The most common is for the ability to be tied to an attribute, and use it as a TN for the roll, with the ability ranks as a bonus. This is especially useful for superhero games. The other method is more common in fantasy and sci-fi games, and involves a special skill being added (typically Magic, or Psionics, or something along those lines). In this case, the activation roll gains a bonus from the skill (which will be defined as using an attribute for the TN) and uses the rank of the ability only to set the EV.


#### Ability Concept/Description

This is the most basic part of the Ability, and is just a brief description of what the ability should be like. Typically this will include a description of what it looks or sounds like, as well as what it actually does. Think of this as the normal language text translation of the following technical information.


#### Manifestation

This is what the ability looks, sounds, smells, tastes, or feels like. It, of course, can also take on more esoteric manifestations, such as a change in color of the user’s aura, or a feeling of psychic pressure. Ultimately, every ability must be perceivable in some way by those being targeted by it, even if that perception is a specialized perception that not everyone has.


#### Properties

A tag, sort of like Power Source, that describes in very basic terms how the ability interacts with the world. For instance, the Fire property means that the ability acts like what we understand as fire: it can ignite cloth and wood, sheds light, and can spread from one source to another. None of this has to be specified in the ability itself, the Fire property takes care of that. Properties that do not have real world counterparts, like Magic or Chaos would have to be defined by the GM. Below is a non-exhaustive list of example properties and the genres they may fit.

*   **Fantasy:** Air, Balance, Chaos, Decay, Earth, Energy, Evil, Fire, Good, Law, Life, Light, Magic, Physical, Plant, Shadow, Water
*   **Horror:** Biological, Divine, Dreams, Infernal, Madness, Mysticism, Spirit
*   **Modern:** Electricity, Magnetism, Radiation, Sound, Technology, Training
*   **Sci-Fi:** Alien, Cosmic Energy, Gravity, Kinetic Energy, Mutant, Psionic, Time


#### Duration

The duration can be Instant, Sustained, or Inherent. Instant is basically what it sounds like, an ability that is used and its effect happens immediately, and then it's over. This does not mean that the consequences are immediately lost, however. Damage is still felt, summoned creatures still exist, etc. It is just that the user does not need to continue to focus on the ability to maintain the effect.

Sustained, however, is exactly the opposite, with the effects and their consequences only existing while focus is maintained. Examples would be a force field that protects those within it, a stealth spell, or creating a telepathic connection in order to communicate non-verbally. These abilities give temporary Focus conditions at the lowest level possible for the entire duration (see the gameplay section below for more information). What this means practically is that a character that uses lots of sustained abilities can also be defeated by confusing them or otherwise damaging their Focus.

The final duration is Inherent, which means that the ability is always on, as it is just part of the character. This is often used in superhero games, but could also model an enchantment like Achilles’ invulnerability in a fantasy setting, or the biological ability for an alien species to breathe underwater. Inherent abilities do not take up Focus condition slots, however they also cannot target other characters. They are personal only.


#### Special Rules

Between the three types of damage conditions and the possibility of giving penalties and bonuses from abilities, most abilities that can be imagined can be created. There are a handful of situations, however, that do not fit within these simple rules.

For these other situations, there is a system of generic conditions: Simple, Minor, Major, and Extreme. These are used for the majority of other effects, such as curses that transform a target into a monster, or mental abilities that can forcibly extract information, mind control, or create illusions.

Simple conditions should generally be mostly cosmetic and not change the way the character is played in any meaningful way. Minor conditions would be weak diseases or poisons, or being changed into a creature that can still manipulate its environment, but maybe not as well as a human. Major conditions are much more devastating, drastically changing the character and their play style, like a curse that changes a character into a cat, or a deadly disease. Finally, Extreme conditions are things like being turned into stone, or a poison with no antidote.

The golden rule is that any ability should have a way to be undone, however. Curses can be lifted, illusions disbelieved, and mind control broken.


#### Setting Costs

Once everything else has been determined, it is time to set the costs for each rank of the ability. Unlike attributes and skills, abilities do not have one unified base cost, although they do follow the same increasing cost pattern as skills and attributes, in which the 3rd rank costs 3 times the base cost, while the 4th rank costs 4 times the base cost.

All abilities start with a base cost of 3 CP, and can be modified from there based on how easy or hard they are to use and what advantages and disadvantages are associated with them. The base cost must be at least 1 CP, and should not be more than 5 CP. Generally speaking, if the ability seems better than the average ability build (it is area of effect, has a rarer than normal defense, or is able to ignore cover, as examples) then increase the cost by one. On the other hand, if the ability is generally worse than the average ability of its type (only targets a specific subset of creatures, ineffective against common defenses, or requires more than a Simple Action to activate, as examples) then reduce the cost.

For examples of abilities for multiple genres, see the Appendices.


### Advantages

In many ways, Advantages are more limited Abilities. With only one rank and a very straightforward application (the majority of them require no activation roll, for instance), Advantages typically give a slight bonus in certain specific situations, or, more rarely, give access to new types of actions.

*   **Attractive:** The character is especially interesting to characters who might be attracted to a character of this gender. This gives the character a +2 bonus on social rolls against those people to befriend, seduce, calm, or deceive them. However, those people are likely to vividly remember the interaction later. This advantage costs 2 CP.
*   **Aura of Command:** The character is especially good at taking control of a situation, and even those not well-disposed towards authority will often fall in line. This grants the character a +2 bonus to social rolls when leading. This advantage costs 2 CP.
*   **Companion:** The character has a companion, be it a servant, slave, friend, or pet. This is the one advantage that has multiple ranks, with the rank setting the PL of the companion, who must be at least 1 PL less than the character taking this advantage. This advantage costs 5 CP per rank.
*   **Danger Sense:** The character, either through extreme paranoia or extreme vigilance, is much harder to ambush than the average person. They are allowed a free Insight roll (typically rolled by the GM) before an ambush. This advantage costs 2 CP.
*   **Encyclopedic Knowledge:** The character is a walking repository of minor facts. Treat the character as having a +1 for any Expertise roll. This does not have any effect on Expertise skills that were purchased or are part of their background, only those skills which the character otherwise does not have any ranks in. This advantage costs 5 CP.
*   **Fearless:** The character is more resistant to fear than others. This could be because of experience or psychosis or any other reason. Discipline rolls again fear get a +2 bonus. This advantage costs 2 CP.
*   **Gigantic:** The character is exceptionally large, measuring over 2 m (roughly 6’6”) in height. This increases both the starting and maximum Strength by 1, grants a +1 to Reach (see the Gameplay section), they take a -2 penalty to Dodge, and they are considered Size 1. This advantage costs 4 CP and must be purchased at character creation.
*   **Hideous:** The character is especially ugly to most members of their species or society. This gives the character a +2 bonus on social rolls against those people, to terrify, intimidate, or otherwise negatively influence those characters. However, those people are likely to vividly remember the interaction later. This advantage costs 2 CP.
*   **Language:** The character is fluent in another language, including reading and writing it if the language has a written form (and the genre/setting features common literacy). The character is automatically considered to have this advantage in their native language. The character does not have to make a Linguistics roll to understand languages she has this advantage for. This advantage costs 1 CP.
*   **Light Sleeper:** The character does not sleep deeply, granting them a Perception roll with a -4 penalty to avoid ambushes during sleep (typically made by the GM in secret). This advantage costs 2 CP.
*   **Perfect Body:** The character is particularly resilient to certain biological stresses, gaining a +2 bonus to resist them. This can take many forms, with common ones being exhaustion, poison, disease, holding their breath, or resisting drugs and alcohol. This advantage costs 1 CP, and can be purchased multiple times, each for a different situation.
*   **Perfect Mind:** The character is particularly good at estimating certain data, gaining a +2 bonus on rolls involving that information. This can take the form of distance, temperature, direction, pitch (as in sound), calculations, or even memory. This advantage costs 1 CP, and can be purchased multiple times, each for a different situation.
*   **Status:** This character has gained some type of higher status in their culture, gaining a +2 on social rolls with people who recognize and respect that status, and a -2 penalty with those who recognize but do not respect that status. Common statuses are military, religious, or governmental ranks, social class, wealth, or fame. This advantage costs 2 CP.
*   **Tiny:** This character is smaller than average, measuring less than 1.5 meters (5’) tall. This reduces the maximum and starting Strength for the character by 1, gives them a -1 Reach, and a +2 bonus on Dodge, and they are considered Size -1. This advantage costs 2 and must be purchased at character creation.


### Gear

While not very important to superhero games, gear is an integral part of most other genres of roleplaying games. Like abilities, VERS takes a very universal, DIY approach to this. Gear is made up of discrete building blocks that are assembled to create weapons, armor, and other equipment. These are built with GP (gear points), although this is only converted to a Character Points cost if the genre or setting does not use money,  resources, or a gear pool to acquire gear.

A gear pool is considered the default method of dealing with gear acquisition in VERS. This pool of points can be used to purchase gear, and gear can be “released” as well, freeing up points for different gear. The gear pool starts at 30 GP, and can be increased by 3 GP for every 2 CP spent on it.

The other official method of dealing with gear acquisition is called Resources. You can think of Resources as a special type of skill, which simulates the character’s money. Gear you wish to purchase has a Value, which is used as a penalty on the Resource roll. If the roll is successful, the character purchases the item, while failure means the character did not have enough funds. Each successful use of Resources gives the character a cumulative -1 penalty on their future uses. These penalties are reduced by 2 at the beginning of every episode.

Resources can be “lent” to allies, with the lender taking a penalty on their Resources and giving their ally an equal bonus on theirs. The debtor character does not regain any Resource bonus until this loan is “paid back,” with the lender character gaining Resources back at double the rate. Characters can also get temporary bonuses to their Resources, called Treasure. These bonuses can be added to a Resource roll to increase the character’s chances of success, but are only used up when the character succeeds. A character can permanently increase their Resources by 1 rank for 3 CP.

Finally, GMs could assign prices to gear in some money unit, and give the players money to spend on them. This is not officially supported, but your game is yours, and I won’t tell you how to have fun.


#### Building Gear

Gear starts from a description, just like abilities do. This description is just the text that explains in normal language what the item is and how it works.

Next the item needs to have its Composition defined (at least if the GM is planning on using the item breakage rules, otherwise not strictly necessary). This is as simple as declaring that the item is made of steel. Most items are made of multiple materials, simply list these in order of their prevalence. A medieval arming sword would be “steel, iron, leather” because the blade is steel, the hilt and pommel are raw iron, and the handle is wrapped in leather strips.

Weapons **_can_** also have a damage type, typically _blunt_, _piercing_, or _slashing_, although others are possible. This is to provide some realism in the interactions between weapons and armor, but is not required. If desiring this extra detail, also give armor a defined weakness, typically one of the above, but others are possible depending on the genre and setting. A weapon is 1.5 times as effective against armor weak against its type.

Finally, assign the desired Components. Components are the building blocks of items, and define the bonuses and penalties associated with their use. The following is a list of the positive and negative Components:

*   **+1 Bonus:** This component gives a +1 bonus of a defined skill roll (Ranged Attack, Athletics, Perception, etc). This skill must be chosen when the item is created and cannot be changed later without GM approval. This component costs 1 GP per rank.
*   **+1 EV/RV:** This component gives a bonus on damage or other effect. This component costs 2 GP per rank.
*   **+1 Hardness:** This component gives the item an additional point of Hardness. Hardness defaults to that of the first element of the composition. This component costs 2 GP per rank.
*   **+1 Mass:** This component increases the Strength required to use the item by 1 (default Strength is 0). Any character using the item with less Strength than this increases takes a penalty on the action equal to the difference in the two. This component reduces the cost by 1 GP per rank.
*   **+1 Reach:** This component gives a bonus to a weapon’s reach in close combat, effectively giving them an advantage when used against weapons with less reach. This component costs 1 GP per rank.
*   **+1 Size:** This component increases the Size of the item (default Size is 0). This gives a -1 penalty per rank on Stealth rolls to conceal the weapon. This component reduces the cost by 1 GP per rank.
*   **-1 EV/RV:** This component reduces the Effect Value of the weapon/gear. Using this, an item can even have a negative EV, essentially reducing the effectiveness of the weapon to the point that it is less effective than the character’s bare hands. Often used to create cursed items. This component reduces the cost by 2 GP per rank.
*   **-1 Hardness:** This component reduces the item’s Hardness by 1. This component reduces the cost by 1 GP per rank.
*   **-1 Penalty:** This component gives a penalty to a defined skill. This skill must be defined when creating the item and can only be changed afterward with GM permission. This component reduces the cost by 1 GP per rank.
*   **-1 Size:** This component reduces the Size of gear (default Size is 0). This gives a +1 bonus on Stealth rolls to conceal the item if the owner character attempts to hide it. This component costs 1 GP per rank.
*   **Additional Type:** This weapon deals more than one type of damage. This component costs 2 GP per rank.
*   **Additional Weakness:** This armor is very specialized, having multiple weaknesses. This makes it more likely to be overcome. This component reduces the cost by 2 GP per rank.
*   **Aerodynamic:** This component allows a thrown weapon to target opponents in the next zone as well as the one the character currently is in without requiring an Athletics roll. This component costs 3 GP.
*   **Ablative:** This component gives its effect 1 time before being rendered useless. In the case of additions to RV, EV just subtracts from this bonus, leaving any leftover RV for another use. This component reduces the cost by 3 GP.
*   **Autofire:** This component allows a weapon to hit multiple times with one attack. The weapon can be used either to make a normal, single attack, a short burst/combo, or a long burst/area of attack. A successful short burst or combo, gains a bonus equal to half again the base EV (1.5 x base EV), but takes a -2 penalty on the attack roll. A long burst or area attack gains the same EV bonus and attack penalty, and it can also target up to 3 targets (GM discretion), using the best reaction of the group as the penalty (if applicable). This component costs 3 GP.
*   **Cannot Use In Vehicle:** This component makes the item unable to be used within a vehicle. This may be due to size, bulk, power requirements, or nearly any other reason imaginable. This would also apply to weapons that cannot be used on horseback, etc. This component reduces the cost by 2 GP.
*   **Chain:** This component means the weapon can be swung or otherwise used so that it negates some of the benefits of cover. Attacks with this weapon reduce the penalty when attacking a character in cover by 2. This component costs 2 GP.
*   **Dull:** This component makes the item have a reduced ability to penetrate the target’s protection, effectively decreasing the severity of any damage conditions by 1 level if the target has any type of armor. This component reduces the cost by 2 GP per rank.
*   **Explosive:** This component allows the EV to effect up to 3 targets in a single zone as well as those that are engaged with them. This component costs 3 GP.
*   **Grab:** This component means the weapon is flexible enough that it can actually be used to initiate a grapple. Its use reduces the penalty for all Strength rolls to maintain grapple by 2. This component costs 2 GP.
*   **Hardened:** This component means the equipment is better designed to thwart technology designed to pierce armor. It could be a radical new engineering principle, better materials, or magic, but the hardened component effectively reduces the severity of an appropriate damage condition by 1 level. This component costs 3 GP.
*   **Limited Uses:** This component limits the number of uses before the item no longer functions, such as running out of ammo. The player must define how the Uses are recovered, such as loading another magazine. The capacity of a weapon is the number of turns it can be used before needing to be reloaded, regardless of the actual shots fired. Once the capacity is 0 then the player either has to reload or find more ammo (depending on the way the weapon was defined to begin with). Capacity starts at 6, although it can be reduced further by purchasing additional ranks. Use of short burst autofire reduces the capacity by 2 for that turn instead, and long burst autofire automatically reduces it by 3. This component reduces the cost by 1 GP per rank. Reloading takes 1 action, unless coupled with the Slow component.
*   **Loud:** This component ensures this item’s use is perceived automatically within 3 zones, and the penalty is reduced by 4 for any perception rolls from further away. This component reduces the cost by 1 GP.
*   **Massive Damage:** This weapon is exceptionally deadly, increasing the severity of the damage condition inflicted by the weapon by 1 level. This component costs 3 GP.
*   **Provides Cover:** This component allows the item to give the character cover, ⅓ per rank. This component costs 2 GP per rank.
*   **Required Hands:** This component means the item requires more than one hand to properly use. There are two levels to this requirement, “Two-handed” and “Hand-and-a-half.” A Two-handed item requires both hands to use at all, and cannot even be attempted otherwise, such as a longbow. An item that is considered hand-and-a-half is best used with two hands, but can be wielded one-handed with considerable effort, increasing the minimum Strength required for its use by 1. This component reduces the cost by 2 GP if it requires both hands, or only 1 GP if 1.5 handed.
*   **Required Skill/Advantage:** This component makes the item only usable if the character also has ranks in a certain skill, or has a specific Advantage. If this completely forbids its use, it reduces the cost by 2 GP per rank, while it only reduces it by 1 GP per rank if it merely gives a penalty.
*   **Requires Vehicle:** This component makes the item only function at all when used in conjunction with a vehicle of some kind. This could be because of bulk, weight, or energy constraints, but whatever the reason, the vehicle is required for the use of the item. This component reduces the cost by 2 GP per rank.
*   **Set:** This component allows the weapon to be braced on the ground charging enemies. Gets a bonus on the attack equal to the bonus from their charge. Typically used to create spears and the like. This component costs 1 GP.
*   **Silent:** This component ensures the item's use is only perceptible within the same zone with an active Perception roll, which has the penalty increased by 4 for each zone away the listener is. Normally items can be heard in the same zone without a perception roll. This component increases the cost by 2 GP.
*   **Slow:** This component makes the item take an extra long time between uses, requiring a whole Turn before being usable again. This component reduces the cost by 2 GP per rank.
*   **Telescopic:** This component allows the weapon to offset the effects of use over extreme ranges. Attacks made with this weapon must be declared if using telescopic or not. If they use the telescopic component, reduce the range penalty on the attack by 2 for each rank (this can reduce the penalty to a maximum of 0). This component costs 3 GP per rank.
*   **Unreliable:** This component makes the item susceptible to jams, breaking, or otherwise ceasing to work at an inopportune time. With each use make a roll to see if the item stops working. At rank 1 the TN for this is 12, but the TN decreases 1 per rank, making the chance of breaking ever more likely. The component reduces the cost by 1 CP per rank.


# Gameplay

The following sections discuss in greater detail how the game works. Much of this information has been hinted at above, but is relayed in greater detail here. This will include rules for conflict, exploration, and investigation.


## Conflict

This is the section of gameplay that many people have the most interest in. However, VERS does conflict differently than most. Conflict in VERS can be mental, physical, or social, and any of the above can be used to defeat enemies, or for your enemies to defeat you.

All conflict is resolved in essentially the same way, however, so this section will cover the general flow of this resolution. After, there will be any special rules for specific aspects of conflict.

All conflict rolls are, at their heart, contests. Because all rolls are made by the player, attacks are made as a skill roll with a penalty based on the opponent’s defense, while if a player character is being attacked they roll their defense with a penalty based on the opponent’s attack skill.  Any degrees of success on the roll can be spent on Stunts (see below for more information). Defense rolls take a cumulative -1 penalty for each additional attack defended against in a turn.

The only real change from a normal contest roll is that a successful attack does something additional. It **affects** the character in some way. Normally this is damage, but these same rules cover ability use as well, and that can mean many different types of effect. The good news is that it doesn’t really matter. All effects work off the same basic framework.

All effects are measured by an Effect Value (EV), which typically consists of the power attribute plus a bonus from a weapon or ability. When an attack is successful, this EV is compared to the target’s Resistance Value (RV). This is typically just their armor or an ability, although in the case of unarmed physical combat or social or mental conflict, this will be the target’s respective resistance attribute.

If the RV is higher, then there is no effect, however if the EV is higher, the difference, or net EV, determines the condition slot this effect takes. Effects always apply a condition at the_ highest slot it qualifies for_. If the slot that it should take is already filled then it upgrades to the next highest slot.

For instance, if the EV is 5, and the RV is 3, then the net EV is 2, which would mean the effect qualifies for only the lowest level condition slot (0-2 net EV), so it goes there. If it had been an EV of 6, however, then it would have applied to the next level up (3-5) instead.

Conditions can only be recovered by taking a Recovery Action. A Recovery Action is a 3d6 roll against the respective resistance attribute TN, although the roll is penalized based on the severity of the highest level condition. Success removes the highest level condition, with additional conditions removed based on degrees of success (see Stunts). Characters get one recovery action per day, typically after sleeping or other lengthy rest.

Certain Skills, such as Meditation and First Aid, allow a character to take a Recovery Action at other times or get bonuses on them.


### Mental Conflict

Most skill rolls will not qualify as mental conflict. To actually use these rules the attacker either has to have intention to confuse, delude, or otherwise cause Focus damage to the target, or their actions are ruled to have this effect by the GM. Mental combat is a fairly unique playstyle, but trying to use these rules for normal actions often leads to situations in which the gameplay does not fit the action in the story, coming across as tedious and contrived, not interesting.

The mental conditions are, in order from least to greatest, _distracted_, _flustered_, _disoriented_, and _distraught_. Distracted and flustered both have two slots, while disoriented and distraught each only have one. For a better description for roleplaying these conditions, see below.

*   **Distracted:** The distracted condition applies if the net EV is between 0 and 2. This is the equivalent of a small diversion of attention, the “hey what's that?” or something similar. Conditions at this level do not impose a penalty on recovery actions.
*   **Flustered:** The flustered condition applies when the net EV is between 3-5. This is the shock of a sudden betrayal, that “et tu Brute?” moment where the character is uncertain of their action for just a split second. Characters take a -1 penalty to all Skills for each condition at this level until the condition is recovered. Recovery Actions take a -1 penalty if this is the highest level Focus condition the character has.
*   **Disoriented:** The disoriented condition applies when the net EV is between 6-8. This is the equivalent of seeing your best friend severely injured, or a character watching in mute horror as the enemy overruns your position. The classic “deer in headlights” level of disbelief. In addition, due to the trauma, any use of skills takes an additional -2 penalty until the condition is recovered. Recovery Actions take a -2 penalty if this is the highest level Focus condition the character has.
*   **Distraught:** This condition applies when the net EV is greater than 9. A character whose Focus is so afflicted is essentially disabled, being too confused to function, overwhelmed by the awesome events transpiring around them. A character with the disarrayed condition can still make actions, although they are restricted to a single action per turn, take a -4 penalty to the action, and must make a Discipline roll to stay conscious. Recovery Actions take a -4 penalty if this is the highest level Focus condition the character has.


### Physical Conflict

Physical conflict is what most gamers are most familiar with. Battle is sometimes unavoidable, and that is when these rules come into play.


#### Turn Order

First is determining Turn order. VERS uses a system often referred to as Popcorn Initiative, in which each player hands off to another player when finished. This hand off is to whomever the first player wishes, so long as they have not yet gone this Turn, and this includes the NPCs and enemies. It is preferred if this hand off can be done in character, such as “Thorbjorn, give me a hand with this guy!” or something similar.

To determine which player goes first, each player should state their case for why they feel they should get that honor. It may be because X tracked the villain, or that Y is his nemesis, or Z is the fastest character. It could also be because W hasn’t gotten to go first yet tonight. The whole table then decides who has the strongest case, with efforts being made to spread the honor around.


#### Movement

VERS does not use a typical movement system. Instead it uses Zones. A Zone is a loosely defined area that is how far a character can move in a single action. If the ground is filled with rubble, then shrink the size of the zone. There is no preset “appropriate” size, although if that is comfortable, somewhere between 5 and 10 meters in diameter. A character can make an athletics roll to move into a second zone in a single move, however, failure on the roll can either leave the character short of their goal, or take up both of their actions, GM discretion.

Once a character has made a Close Combat attack on a target, or been the target of a Close Combat attack, the two characters are considered Engaged. This means that if either character wishes to retreat or otherwise move away from the other, they must Disengage. This requires them to take an action and make an Athletics roll, failure gives the opponent a free attack. If more than two characters are involved, then each additional character adds a -2 penalty to the roll.

Finally, a character cannot move through another character. If the only way out of a zone is through a character or within a reasonable attack range of a character (GM discretion, but a good rule of thumb is 2 meters) then they cannot proceed further. The only exception is with an Athletics roll, taking a penalty equal to the guarding character’s Close Combat ranks. Failure means they take damage and fail to get by. Similarly to the Disengage rules, multiple guard characters increase the penalty (and the possible damage) by 2.


#### Range

When making ranged attacks, targeting characters in the same zone gives a -2 penalty, while attacks to targets in the next zone do not take a penalty. Each zone further away imposes an additional -2 penalty per zone. Finally, making a ranged attack on a character who is currently Engaged in close combat also gives a -2 penalty to the ranged attack, and an additional -2 per additional person engaged.

Thrown weapons can target any enemy in the same zone, or can target foes in the next zone over with a successful Athletics roll, and an additional zone over for every 3 Degrees of Success. This Athletics roll is subject to the same penalties as a ranged combat attack, and substitutes as the attack roll.


#### Reach

Some gear and even some abilities or advantages give a character a longer (or shorter) effective reach. This is a simple system to emulate that. When characters with different reaches come into conflict, the character with the greater reach gets a bonus equal to the different, while the other character gets an equivalent penalty.


#### Cover

Cover can be the difference between life and death in a battle. The VERS cover system is very simple, giving either a -1, -3, or -5 penalty on attacks against targets with cover, based on approximately how much they are covered (⅓, ⅔, or complete). If an attack misses by the amount of the penalty then it strikes the cover instead, and could damage it or even break it.


#### Injury Conditions

The physical conditions are, in order from least to greatest, grazed, wounded, impaired, and disabled. Distracted and flustered both have two slots, while disoriented and distraught each only have one. For a better description for roleplaying these conditions, see below.

*   **Grazed:** The grazed condition applies if the net EV is between 0 and 2. This is the equivalent of the dramatic cheek cut or slicing a Z in their shirt, and does not really impact a character in any other meaningful way. Conditions at this level do not impose a penalty on recovery actions.
*   **Wounded:** The wounded condition applies when the net EV is between 3 and 5. This is the equivalent of a flesh wound, a dull weapon, punch, kick, etc, would give the character a bruise while a cutting or piercing attack would be a minor cut that bleeds but is not enough to slow or otherwise harm the character. Characters take a -1 penalty to all Skills for each condition at this level until the condition is recovered. Recovery Actions take a -1 penalty if this is the highest level Injury condition the character has.
*   **Impaired:** The impaired condition applies when the net EV is between 6 and 8. This is the equivalent of a broken bone for dull types of attacks or a well-placed gunshot or stab. In addition, due to the trauma, any use of skills takes an additional -2 penalty until the condition is recovered. Recovery Actions take a -2 penalty if this is the highest level Injury condition the character has.
*   **Disabled:** This condition applies when the net EV is greater than 9. A character who gets hit by this much damage (or has lesser damage upgraded to this point) is disabled in a dramatically appropriate way. This could be physical unconsciousness, or death, depending on what type of damage it was. A character with the disabled condition can still make actions, although they are restricted to a single action per turn, take a -4 penalty to the action, and must make a Discipline roll to stay conscious. Recovery Actions take a -4 penalty if this is the highest level Injury condition the character has.


#### Breaking Stuff

In addition, objects also have conditions based on their size and hardness. Attacks on inanimate objects only take penalties if they are in motion or are small (such as a baseball). Otherwise it is a normal skill roll to make the attack. The EV of the attack is compared to the Hardness of the object, and the remainder, if any, determines the conditions, explained below.

| Hardness Examples        |
|:----:|:-----------------:|
| Rank | Example Material  |
| -5   |       Butter      |
| -2   |       Flesh       |
| -1   |    Cloth, paper   |
|  0   |   Leather, rope   |
|  1   |   Glass, pottery  |
|  2   |     Wood, bone    |
|  3   |                   |
|  4   |       Bronze      |
|  5   |    Stone, iron    |
|  6   |   Tempered steel  |
|  7   |                   |
|  8   |      Diamond      |

The object conditions are, in order from least to greatest, worn, damaged, tattered, and broken. Each condition only has one slot. Objects can’t take recovery actions, but can be repaired with the right background or special skill, otherwise the process is the same.

*   **Worn:** The Worn condition applies if the net EV is between 0 and 2. These are small nicks, fraying, or other such small damage. This can be seen but does not impede the item's function. This level does not impose a penalty on repairs actions.
*   **Damaged:** The Damaged condition applies when the net EV is between 3 and 5. Knicks, cracks, rust, holes; these are the signs of the Damaged condition. Characters using the item take a -1 penalty at this level until the condition is repaired. Repair Actions take a -1 penalty if this is the highest level Damage condition the object has.
*   **Tattered:** The Tattered condition applies when the net EV is between 6 and 8. A tattered item is so damaged it is barely holding together, with large gashes, missing chunks, or other major signs of damage. In addition, due to the damage, any use of the item takes an additional -2 penalty until the condition is repaired. Repair Actions take a -2 penalty if this is the highest level Damage condition the object has.
*   **Broken:** This condition applies when the net EV is greater than 9. A broken item no longer functions but is in total disrepair. Any function that the object once had is lost, and if that function was structural then it crumbles, shatters, or otherwise physically comes apart, possibly with disastrous results.


#### The Chase

A common type of physical conflict in movies, novels, and video games, a chase is simply when one party, here referred to as the quarry tries to evade a second party, referred to as the hunter seeks to catch them. A chase can have three different permutations, and these stages can morph and change one into another depending on events. These permutations are interposing, pursuit, and tracking.

Interposing is often the final state of a chase, and represents when the hunter has gotten within a single action of the quarry. Alternatively, interposing can happen when two parties are both seeking a stationary target and wish to keep the other from achieving it. Either way, this stage is resolved with an Athletics roll, with a penalty equal to the NPC’s Athletics. If the player is the hunter, then success means the quarry is captured, if the player is the quarry, then success means they dodged or otherwise evade the hunter, and if the target is a mutually desired object then the player has taken control of the object.

Pursuit is the middle state, although it is often where the chase begins. At this time the hunter can still see the quarry (or otherwise actively sense them), but they are not close enough for a final attempt to bring them down. The GM needs to decide how many actions they want the pursuit to take, between 2 and 6. This creates a scale equal to double the length, in which both parties start on a central count. For this reason, your final scale should be an odd number. For example, on the shortest length, the scale is 5 positions long and the parties typically start at 3, while on the longest length the scale is 13 positions and the parties typically start at 7.

If the player is the hunter, each successful Athletics roll (penalized by the quarry’s Athletics) moves them down (toward 1) while failures move them higher. If the player is the quarry, the same roll would be made, but success takes them higher, while failure takes them lower. Once the character gets to 1, they may attempt to Interpose, while reaching the other end of the scale means the quarry has gotten away, possibly transitioning to the tracking phase.

It is important to note that each of these turns should be described and narrated, and not just treated as sterile rolls. The GM should impose bonuses or penalties based on tactics taken by the players. Pulling down bookshelves may give the hunter a penalty, as might heading into a crowded street festival, while the quarry may take a penalty if the hunter takes a shortcut, crashes through a window, or fires their firearm into the air to clear the street.

The final phase or stage is tracking, and at this point the hunter can no longer see or otherwise sense the quarry directly. To continue to the chase at this point, the hunter must have a way to do so, such as Survival or Investigation. This takes a similar structure as pursuit, but each roll is made for an hour’s worth of seeking. Hunters roll their Survival or Investigation to try and move closer to the quarry while the quarry rolls Stealth to leave as little trail as possible. If the player gets to 1 then contact is made and Pursuit can begin again, while if it gets to the end of the scale then the quarry is completely away.


### Social Conflict

Much like mental attacks, not all social skill rolls should use the social conflict rules. Attempting to simply convince a character to let you in, or to trick a character into attacking an enemy are not inherently social combat. The attacker has to intend to actually harm the Morale of the target for it to qualify, such as attempting to anger, frighten, sadden, or embarrass, or for the action to inadvertently cause such emotional harm. This is up to the GM’s final say, however.

The social conditions are, in order from least to greatest, unnerved, shaken, disheartened, and demoralized. Unnerved and Shaken both have two slots, while disheartened and demoralized each only have one. For a better description for roleplaying these conditions, see below.

*   **Unnerved:** The unnerved condition applies if the net EV is between 0 and 2. This is the equivalent of feeling slightly nervous or agitated, such as when hearing a noise at midnight when you are supposed to be home alone or getting called on in class while daydreaming. That feeling of butterflies in your stomach. Conditions at this level do not impose a penalty on recovery actions.
*   **Shaken:** The shaken condition applies when the net EV is between 3 and 5. This is the shock of something serious, like getting a glimpse of something waiting in the shadows, your crush blabbing your secrets to the school bully and his friends. The feeling of your heart skipping a beat. Characters take a -1 penalty to all Skills for each condition at this level until the condition is recovered. Recovery Actions take a -1 penalty if this is the highest level Morale condition the character has.
*   **Disheartened:** The disheartened condition applies when the net EV is between 6 and 8. This is the equivalent of being surrounded and outnumbered by enemies, or your best friend telling you that he was only using you to get help with his homework. Horror but not yet despair. In addition, due to the trauma, any use of skills takes an additional -2 penalty until the condition is recovered. Recovery Actions take a -2 penalty if this is the highest level Morale condition the character has.
*   **Demoralized:** This condition applies when the net EV is greater than 9. A character whose Morale is so afflicted is essentially disabled, being too terrified or otherwise overcome to function, overwhelmed by the awesome events transpiring around them. A character with the demoralized condition can still make actions, although they are restricted to a single action per turn, take a -4 penalty to the action, and must make a Discipline roll to stay conscious. Recovery Actions take a -4 penalty if this is the highest level Morale condition the character has.


## Exploration

Exploration is both journeying between settlements and investigating ruins and other adventure sites. The majority of these rules deal with handling environmental dangers. Also note that certain genres, such as historical and fantasy will use these rules more than modern and sci-fi games.


### Poisons and other Chemicals

Venomous snakes, poison dart traps, and enchanted pools that suck your soul out of your body. No matter what their form, poisons and chemicals are a frequent danger in the wilderness areas that characters often find themselves in. Poisons and chemicals have three major defining features: Concentration, Potency, and Vector.

Concentration determines how much of the harmful agent is present in the substance and can be dilute, moderate, or concentrated. This determines how often the damage is applied to the target.

Potency is how strong of a reaction the harmful agent produces, and can be weak, moderate, or strong. This determines the base EV of the substance.

Vector is how the harmful agent has to be applied to a target to take effect, and can be contact, inhaled, ingested, or injected.. A substance only harms the target if the target encounters it by one of its Vectors. A substance can have multiple Vectors

The substance affects the target once on the action in which they first encounter it, and then again after every time period indicated by the Concentration, unless they have successfully removed it (GM discretion). In addition, this EV doubles with each passing period, gradually becoming worse and worse. The following chart describes this in greater detail.

| Chemical and Poison: EV over Time                               |
|:------------:|:--------------:|:--------------:|:--------------:|
|              |      Weak      |    Moderate    |     Strong     |
|    Dilute    | 1 EV, per hour | 2 EV, per hour | 4 EV, per hour |
|   Moderate   |  1 EV, per min |  2 EV, per min |  4 EV, per min |
| Concentrated | 1 EV, per turn | 2 EV, per turn | 4 EV, per turn |

The only special caveat to this is that poisons (including drugs and alcohol) must be administered in a high enough dose at one time, **_or_** have the effective dose administered before the system metabolizes the poison. This dose is generically set to double the Potency within a number of hours equal to Stamina, although the GM may determine unique dosage rules if desired for each poison.


### Exposure

When subjected to extreme heat or extreme cold, survival becomes increasingly difficult. Humans are able to exist easily in only a very narrow band of temperatures, roughly 10ºC up to around 30ºC can be experienced for long periods without specialized garments or equipment. However, for every 10ºC above or below that range, the character is in increasing danger.

For every hour they spend exposed to those temperatures they are subjected to a cumulative 1 EV of damage per 10ºC. In other words, the first hour at 0ºC is a 1 EV, the second hour is 2 EV, the third is 3 EV, etc. This damage affects all three conditions, but is resisted by their respective resistance attribute (Resolve, Stamina, or Composure). A wet character is considered to be experiencing 10ºC colder temperatures than the actual thermometer reads.

The character cannot recover from these conditions until they are out of the elements.


### Thirst and Starvation

A character can go a number of days without water equal to their Stamina +1 (minimum of 1 day). After this point, the character takes a cumulative 1 EV damage to all three trackers per day without water, resisted by the respective resistance attributes. This means the first day would be 1 EV, the second would be 2 EV, the third would be 3 EV, etc.

A character can go a number of days without food equal to their Resolve + their Stamina +2 (minimum of 2 days) before the damage begins. Otherwise it progresses like thirst. That said, they are two separate processes (as are exposure) and so each must be accounted for individually.

Like with exposure, a character cannot recover from these conditions until they have drank and/or eaten.


### Suffocation

A character outside of conflict can hold their breath for a number of minutes equal to half their Stamina (minimum 30 seconds). At this point, they must succeed on a Discipline roll every Turn or take a cumulative +1 EV Focus attack. They do not get their Resolve against this damage. This roll takes an additional -1 penalty every Turn. Once the character loses all of their Focus, they automatically gasp and begin drowning or  breathing in noxious fumes, etc. In the case of fumes, they are now being affected by whatever that poison is, per the poison rules. In the case of lack of oxygen, they instead keep taking damage, although it changes to Injury. The EV does not reset, and Stamina is not used as an RV. Once their last Injury condition is marked the character has suffocated or drowned.

In combat or when doing strenuous activity (GM discretion), the character has considerably less time. Instead of minutes, the character can hold their breath for a number of Turns equal to their Stamina (minimum 1) before having to make Discipline rolls. At this point the situation progresses largely the same.


### Falling

A character making a controlled fall (i.e. they jumped or dropped purposefully) faces 1 EV per meter fallen, although they can make an Athletics roll with a -1 penalty per meter to halve that. The character can apply Stamina as their RV, but armor does not apply. Other abilities may add to the RV at GM discretion.

An uncontrolled fall is much more deadly, with an EV of 2 per meter fallen. The character can still make an Athletics roll, but starts with a -4 penalty and still takes the -1 penalty per meter.


### Visibility

If the character is in a dark environment, they take a penalty on their vision based Perception rolls according to the amount of light available. For reference, a modern city street would give a -2, while an ancient city or modern rural area would give a -4. Wilderness with a full moon would be a -5, and wilderness with full cloud cover would be a -6. A cave or inside a building with no windows is impossible.

Weather can also affect perception, with light fog or moderate rain or snow giving a -1 penalty to sight, hearing, and scent per zone away. Moderate fog or heavy precipitation gives a -2 penalty to sight, hearing, and scent per zone. Heavy fog gives a -3 penalty per zone. Weather related penalties also apply to Survival rolls.


## Investigation

Investigation means things like searching for clues, trying to solve a mystery, or simply looking for secret passages or treasure. There are not any specific rules for this type of gameplay, but a small amount of advice.

First, for players, always be specific when telling the GM you are investigating. The GM cannot know if you will find the clue if you are not specific in your intentions. Also, do not tell the GM you are making a roll to investigate. The GM will tell you what to roll, if anything, after you have made clear your intentions. The reason is that not every action needs a roll, and only the GM can decide when that is the case.

For GMs, remember that not all actions need rolls. If the character is an expert in ancient latin, then they should just be able to read the ancient latin inscription. Also, a character who is a mathematician should be able to recognize geometric symbols, etc. Remember, only make the player roll if there is both a chance to fail, and the failure would be interesting.


## Stunts

As mentioned back in the Basics chapter, rolls in VERS are not just about determining success, but also giving variation to the levels of success. Every roll generates Degrees of Success which can be spent on Stunts, earning the ability to further define the way the success looks or by gaining specific advantages. The character just determines how they want to spend their Degrees of Success to enhance the roll effect, and the GM uses that information to enhance the narration of the events in addition to the in-universe benefits.

The following tables contain the available Stunts, not just for combat, but also miscellaneous roleplay situations. These, of course, are not the only stunts possible. Any time a player wants to do something interesting in combat they should be encouraged. Use these stunts as examples to empower interesting combat and tactical thinking.


### Physical Conflict - Offensive Stunts

The above stunts are primarily tied to the Close Combat and Ranged combat skills, turning good rolls into bonuses of different kinds for the character and their allies. Because of the sheer number of stunts related to physical conflict, they are split between offense and defense. Below are more detailed descriptions of the offensive physical stunts.

| Physical Conflict - Offensive Stunts                                                             |
|:---:|-------------------|------------------------------------------------------------------------|
| DoS | Name              | Effect                                                                 |
| 2+  | Defensive Strike  | Reduce penalties on reaction rolls by 1 until your next turn           |
| 2   | Hamper            | Give the target a -1 for actions involving hands or arms               |
| 2   | Maim              | Give the target a -1 for actions involving legs or feet                |
| 2+  | Power Strike      | Increase the EV of this attack by 1                                    |
| 2+  | Precision Strike  | +1 for your next attack roll                                           |
| 2   | Provoke           | Target must attack you on its next turn, penalties based on Presence   |
| 3+  | Assist            | Give designated ally + 2 to attack this foe                            |
| 3   | Combo             | Treat attack as if it had the Autofire gear component                  |
| 3   | Disarm            | Remove target’s weapon unless Strength is 2 ranks higher than yours    |
| 3   | Guerrilla Tactics | Disengage from the target without needing to make Athletics roll       |
| 3   | Knockback         | Force the target to disengage                                          |
| 3   | Suppression       | Automatically attack engaged or designated target if they act          |
| 4   | Choke Out         | Grapple Only: Target becomes Disabled, based on their Stamina          |
| 4   | Head Strike       | Increase EV by 2 and do an equal amount to Focus                       |
| 4   | Human Shield      | Grapple Only: Target grants ⅔ cover                                    |
| 4   | Piercing Strike   | Reduce the target’s RV by 2 for this attack                            |
| 4   | Rend Armor        | Reduce the target’s RV by 1 until they readjust armor (Complex Action) |
| 4   | Restrain          | Grapple Only: Target cannot move, you must have proper gear            |
| 4   | Trip              | Knock your enemy to the ground, based on Agility                       |

*   **Assist:** Whether it be through distraction or through some other course of action, the character’s actions give a designated ally a +2 bonus to attack this foe on that ally’s next turn. This stunt costs 3 Degrees of Success and can be chosen multiple times, each granting a different ally the benefit.
*   **Choke Out:** Grappling Only. The character uses his weight and leverage to briefly cut off the target’s oxygen supply. The target of this grappling attack becomes Disabled if their Stamina is less than the user's Strength, otherwise merely increase the EV of the attack by 2. This stunt costs 4 Degrees of Success.
*   **Combo:** The character’s speed allows them to get in a second attack. Treat this attack as if it had the _Autofire_ gear component. This stunt can only be chosen once per Turn, and costs 3 Degrees of Success.
*   **Defensive Strike:** Using quick strikes and keeping their guard up, the character gains a +1 bonus on reactive rolls until their next Turn. This stunt costs 2 Degrees of Success and may be chosen multiple times.
*   **Disarm:** Whether by brute force or skilled finesse, the character knocks the weapon from the target’s grasp unless the target's Strength (or Agility) is 2 or more ranks higher than that of the user. The Attribute compared is the highest for the attacker. This stunt costs 3 Degrees of Success.
*   **Guerrilla Tactics:** Through fancy footwork or skilled distraction, the character keeps their opponent off guard, allowing them to immediately disengage from their target. This disengagement does not require the standard Athletics roll. The stunt costs 3 Degrees of Success.
*   **Hamper:** The character strikes their target’s arm or hand, giving a wound that makes using it more difficult. Give the target a -1 penalty for actions involving their hands or arms, such as attacking. If they are attacking a player character, give the player a +1 to defense instead. This penalty lasts until the end of the combat. This stunt costs 2 Degrees of Success.
*   **Head Strike:** The character’s attack connects with the target’s head, dealing a greater than normal amount of damage and potentially dazing them. Increase the EV of this attack by 2 and deal an equal amount to Focus. The stunt costs 4 Degrees of Success.
*   **Human Shield:** Grappling Only. The character maneuvers their target into position so that they are between the character and the remainder of their enemies. Their body grants the character ⅔ cover, although without a weapon with which to threaten the target (a blade to the throat or a gun to their head) this positioning gives them the advantage on breaking out of the grapple on their next turn. Increase the penalty to maintain the grapple by 2 in this case. A target who is threatened with a weapon who tries to get away suffers an immediate attack from the weapon. This stunt costs 4 Degrees of Success.
*   **Knockback:** The power, leverage, or angle of the character’s attack takes the target by surprise either pushing them backwards or making them take several steps to regain their stance. This stunt forces the target to disengage, although they do not suffer any additional attacks for the movement. They do, however, have to spend a Simple Action to re-engage in close combat. This stunt costs 3 Degrees of Success.
*   **Maim:** The character strikes their target’s legs or feet, giving a wound that limits their mobility. The target takes a -1 penalty for any Athletics rolls to disengage or move more than one zone. This penalty lasts until the end of the combat. This stunt costs 2 Degrees of Success.
*   **Piercing Strike:** The character’s attack finds a particularly weak point in their opponent’s armor, ignoring some of its protection. Reduce the target's RV by 2 for this attack. This stunt costs 2 Degrees of Success.
*   **Power Strike:** The character lands a particularly heavy blow, dealing additional damage. Increase the EV of this attack by 1. This stunt costs 2 Degrees of Success  and can be chosen multiple times.
*   **Precision Strike:** The character’s attack sets them up for a deadly second attack, putting them in a position to strike even more accurately. The character’s next attack gains a +1 bonus. This stunt costs 2 Degrees of Success and can be chosen multiple times.
*   **Provoke:** The skill and/or power of the character’s attack leaves the target emotionally open for a taunting comment, wounding their pride and enraging them. The target must attack the character who struck this blow on their next turn. In addition the character gets a +2 bonus to reaction rolls related to that attack if the character’s Presence is greater than the target’s Composure. This stunt costs 2 Degrees of Success.
*   **Rend Armor:** The character’s attack manages to disrupt, break, or otherwise temporarily open up a sizable weakness in their defenses. Reduce the target's RV by 1 for all following attacks until they take a Complex Action to readjust their armor. This stunt costs 4 Degrees of Success.
*   **Restrain:** Grappling Only. The character is able to handcuff, tie up, or otherwise incapacitate the target. The target of this grappling attack is unable to move, use their hands, or their voice, depending on the nature of the restraint. The user must have a method to appropriately restrain the character available when choosing this stunt. This stunt costs 4 Degrees of Success.
*   **Suppression:** The character’s attack puts them into position to strike an enemy that moves or acts within their reach. The next action taken before the character’s next turn by an opponent that is engaged in close combat with them (or a single designated target within sight for a ranged attack) grants the character an automatic attack action. This stunt costs 3 Degrees of Success.
*   **Trip:** Whether by sheer power or carefully designed maneuvering, the character’s attack knock’s the target to the ground unless the target's Agility is 2 or more ranks higher than that of the character. This stunt costs 4 Degrees of Success.


### Physical Conflict - Defensive Stunts

The following are in depth descriptions of the stunts available to a character defending against a physical attack. Many of these are only available after using a specific defense, such as parry or Dodge, although the GM may decide that other methods could work if dramatically appropriate.

| Physical Conflict - Defensive Stunts                                                        |
|:---:|-----------------|---------------------------------------------------------------------|
| DoS | Name            | Effect                                                              |
| 2   | Force Overreach | Reduce the DC of your next attack roll against the attacker by 2    |
| 2   | Take Cover      | Throw yourself into cover to avoid area-of-effect or ranged attacks |
| 2   | Taunt           | The enemy that attacked you must attack again on its next turn      |
| 3   | Disarm          | Following successful parry, remove attacker’s weapon                |
| 3   | Guardian        | Following successful parry, grant ally cover with your body         |
| 3   | Overwatch       | The first action by engaged enemies provokes an automatic attack    |
| 3   | Sacrifice       | Allow unsuccessful attack to hit to deny attacker defence next Turn |
| 3   | Shove Back      | Following successful parry, force target to disengage               |
| 3   | Withdraw        | Your movements allow you an opening to disengage without penalty    |
| 4   | Disrupt Balance | Your movements force your attacker to the ground                    |

*   **Disarm:** Whether it is through brute strength or skillful maneuvering, your parry deprives the attacker of their weapon, leaving them barehanded. Following a successful parry, the attacker's weapon is removed from their grip unless the target's Strength (or Agility) is 2 or more ranks higher than the corresponding Attribute of the character. The Attribute used is based on the one the highest for the defender. This stunt costs 3 Degrees of Success.
*   **Disrupt Balance:** Skillful maneuvering leaves the attacker in a vulnerable position. The attacker is knocked prone unless the target's Agility is 2 or more ranks higher than the defender. This stunt costs 4 Degrees of Success.
*   **Force Overreach:** Either with subtle footwork or blind chance, the attacker not only misses but leaves themselves wide open for a counterattack. Reduce the DC of any character’s next attack roll against the attacker by 2. This stunt costs 2 Degrees of Success.
*   **Guardian:** The ultimate altruism, the character moves to defend an ally with their physical body. Following a successful parry, the user positions themselves to grant full cover to an ally within the same zone with their body. This movement does not require the normal Athletics roll, and the character is disengaged from their attacker unless the character to be given cover is also engaged with the same enemy. This stunt costs 3 Degrees of Success.
*   **Overwatch:** Due to the character’s vigilance, the character threatens those enemies within their reach. The first action by a target the character is engaged with (or a designated target the character can see if using a ranged attack) before the next turn provokes an automatic attack from you. This stunt costs 2 Degrees of Success.
*   **Sacrifice:** The character makes a devil’s bargain, allowing themselves to be hit by an otherwise unsuccessful attack in order to deny the attacker their defense on the next attack. This stunt costs 3 Degrees of Success.
*   **Shove Back:** The impact of the character’s parry pushes the attacker back and forces the target to disengage. This stunt costs 3 Degrees of Success.
*   **Take Cover:** The character takes evasive action, immediately throwing themself into the nearest cover. This reduces the damage taken from area-effect attacks and grants cover against future ranged attacks, however the character is left prone. In addition, the GM can declare that there isn’t any cover, in which time the Degrees of Success are not spent, or the GM allows merely being prone to grant ⅓ cover. This stunt costs 2 Degrees of Success.
*   **Taunt:** The character touts their defensive capabilities, embarrassing and enraging the attacker. This attacker must attack the defender again on their next turn. In addition, the defender gains a +2 to reaction rolls against the target if their Presence is 2 ranks greater than the attacker’s Composure. This stunt cost 2 Degrees of Success.
*   **Withdraw:** Due to the character’s movements they are left an opening to disengage  from the enemy without allowing for a counter attack. The character disengages from the target without making the standard Athletics roll. The stunt costs 3 Degrees of Success.


### Ability Stunts

The following stunts apply to actions involving the activation, targeting, or other use of abilities. Ability use can also draw upon stunts from any other lists that make sense for the action being attempted, however.

| Ability Stunts                                                                                  |
|:---:|---------------------|---------------------------------------------------------------------|
| DoS | Name                | Effect                                                              |
| 2   | Defensive Use       | +2 to your reactions until next Turn                                |
| 2+  | Empower Ability     | Increase the EV of this attack by 1                                 |
| 3   | Area of Effect      | Single target ability affects 3 targets and those engaged with them |
| 3   | Disguise Ability    | Prevent enemies from discerning ability type unless Logic is higher |
| 3   | Impressive Display  | Witnesses of amazing feat gain bonuses or penalties                 |
| 3+  | Multiple Targets    | Allow a single target ability affect 1 additional target            |
| 4   | Penetrating Ability | Reduce the target’s RV for this attack by 2                         |
| 4   | Quicken Ability     | Allow another ability to be used in the same Simple Action          |
| 5   | Shape Ability       | Allow area-of-effect abilities to strike only enemies               |

*   **Area of Effect:** The character is able to push the ability beyond its normal limits, striking multiple targets. Allow a normally single target ability to instead strike up to three targets and those engaged with them. These targets must all be within the same zone. This stunt costs 3 Degrees of Success.
*   **Defensive Use:** Through care and vigilance, the character uses their ability while maintaining a defensive posture. The character gains a +2 bonus to reaction rolls until their next Turn. This stunt costs 2 Degrees of Success.
*   **Disguise Ability:** Careful to avoid those that might disrupt their ability, the character activates it in a way that is subtle. This stunt prevents enemies from discerning ability type (for those trying to counter the ability using dispel type abilities, etc) unless their Logic is 2 or more ranks higher than the user. This stunt costs 3 Degrees of Success.
*   **Empower Ability:** The character puts more energy than normal into the ability, making the effect stronger than normal. Increase the EV of the ability by 1 for this use. This stunt costs 2 Degrees of Success and can be chosen multiple times.
*   **Impressive Display:** Whether it is an excessive show of force, a rousing speech, or a carefully choreographed set of poses to accent their ability activation, the character leaves their opponents stunned. Enemies who witness it take a -2 penalty on their next actions, or if they are actions against a player character, the player character gains a +2 bonus. This stunt costs 3 Degrees of Success.
*   **Multiple Targets:** The character skillfully manages to strike more than one enemy with this attack. The enemies do not have to be near each other for this to work, and there is no more risk to hit allied characters than normal for engagement. Allow a normally single target ability to strike an additional target. This stunt costs 3 Degrees of Success and can be chosen multiple times, each adding an additional target.
*   **Penetrating Ability:** Perhaps the ability strikes in just the right way to get through the target’s armor, or perhaps it’s some mystical resonance of souls that weakens their magical barriers, regardless, this stunt reduces the target's RV against the ability by 2. This stunt costs 4 Degrees of Success.
*   **Quicken Ability:** Through efficient use of shortcuts or perhaps just through rage, the character unleashes a particularly fast flurry of abilities. This stunt allows the character to activate another ability in the same Simple Action. This stunt costs 4 Degrees of Success and can only be used once per turn.
*   **Shape Ability:** With concentration and skill, the character is able to shape an area-of-effect ability to only strike enemies. Otherwise it acts the same as normal. The stunt costs 5 Degrees of Success.


### Recovery Stunts

The following stunts pertain exclusively to the recovery action, enhancing or changing it in some way. Some of these only apply to the character using making the roll while some them can only target others.

| Recovery Stunts                                                                                   |
|:---:|-----------------|---------------------------------------------------------------------------|
| DoS | Name            | Effect                                                                    |
| 2+  | Center Self     | Meditation skill reduces the penalty of next Focus recovery by 1          |
| 2+  | Soothe          | Influence skill reduces the penalty of target’s next Morale recovery by 1 |
| 2+  | Tend Wound      | First Aid skill reduces the penalty of target’s next Injury recovery by 1 |
| 3+  | Calming Spirits | You recover from one additional Morale condition, from worst to least     |
| 3+  | Coming Clarity  | You recover from one additional Focus condition, from worst to least      |
| 3+  | Healing Rest    | You recover from one additional Injury condition, from worst to least     |

*   **Calming Spirits:** The character relaxes and their spirits lift, allowing them to remove an additional Morale condition during this recovery action, going from worst to least. This stunt costs 3 Degrees of Success and can be chosen multiple times.
*   **Center Self:** The character’s meditations help to calm them and clear their minds. The character reduces the penalty for their next Focus recovery roll by 1. This stunt costs 2 Degrees of Success and can be chosen multiple times.
*   **Coming Clarity:** The character clears their mind and refocuses on the tasks at hand, allowing removal of an additional Focus condition during this recovery action, going from worst to least. This stunt costs 3 Degrees of Success and can be chosen multiple times.
*   **Healing Rest:** The character’s rest rejuvenates them, easing pains and stitching together wounds, allowing the character to remove an additional Injury condition during this recovery action, going from worst to least. This stunt costs 3 Degrees of Success and can be chosen multiple times.
*   **Soothe:** Calming words and comforting actions help to lift the target’s spirits and inspire them to keep going. The target reduces the penalty for their next Morale recovery roll by 1. This stunt costs 2 Degrees of Success and can be chosen multiple times.
*   **Tend Wound:** Bandages, splints, and other basic first aid let the target shrug off the effects of their wounds. The target reduces the Penalty for their next Injury recovery action by 1. This stunt costs 2 Degrees of Success and can be chosen multiple times.


### Exploration and Investigation Stunts

The following stunts primarily apply to actions taken when exploring a site, traveling in the wilderness, or when investigating a mystery, although it’s possible situations may come up in other types of scenes.

| Exploration and Investigation Stunts                                                          |
|:---:|-------------------|---------------------------------------------------------------------|
| DoS | Name              | Effect                                                              |
| 2   | Adaptable         | Improvise tools for action you are otherwise unequipped for         |
| 2   | Cross Reference   | The discovered information gives +1 on next related roll            |
| 2   | Distraction       | Reduce penalty on allies next Stealth roll by 2, you are discovered |
| 2   | Eureka            | Find an additional fact regarding discovered information            |
| 2   | Shadow            | While still undetected gain +2 for your next action                 |
| 2+  | Tracking          | Survival roll reveals a fact about the quarry                       |
| 3   | Advanced Scouting | Discover enemies from most advantageous position                    |
| 3   | Ghost             | The character leaves no trace, increasing the penalty to find by 2  |
| 3   | Sacrifice         | Take environmental damage instead of ally                           |
| 4   | Panache           | Your confidence reduces the penalties on future related rolls by 1  |
| 4   | Scapegoat         | You plant evidence pointing to someone else’s guilt for this action |
| 4   | Treasure Hunter   | Find a small cache of loot, increasing Treasure by 1                |

*   **Adaptable:** The character uses the supplies they do have along with materials in their environment to improvise a tool for an action they are otherwise unequipped for. This halves the penalty penalty for being improperly equipped for an action, reducing it by a minimum of 1. This improvised tool is broken in the process. This stunt costs 2 Degrees of Success.
*   **Advanced Scouting:** The character’s skill while scouting ahead allows them to locate enemies or potential enemies from a particularly advantageous position. This typically means the character can see them and they cannot see the character, although other advantages are possible. This stunt costs 3 Degrees of Success.
*   **Cross Reference:** The character’s investigations are particularly fruitful, with the clue, evidence, or discovered information giving them a lead on further clues, giving the next related roll +1. This stunt costs 2 Degrees of Success.
*   **Distraction:** The character draws the attention of enemies in the area, providing a chance for the rest of the team to remain undetected. This reduces the penalties of the next Stealth based roll by 2 for all allies. This stunt costs 2 Degrees of Success.
*   **Eureka:** The character’s investigation has been particularly fruitful, turning up more information than originally expected. The GM reveals a further fact about the discovered clue or evidence. This stunt costs 2 Degrees of Success.
*   **Ghost:** The character’s actions are so skillful and stealthy they manage to leave no trace of this action, increasing the penalty for any roll used to detect or trace their presence by 2. This stunt costs 3 Degrees of Success.
*   **Panache:** The character succeeds with an ease that boosts their confidence, reducing the penalties for related rolls by 1 for the remainder of the episode. This stunt costs 4 Degrees of Success, and can be chosen by a character only once per an episode.
*   **Sacrifice:** The character altruistically pushes an ally to safety, taking the damage for them. If environmental damage would affect an ally the character can elect to take this stunt to swap places and take the damage in their stead. This stunt costs 3 Degrees of Success.
*   **Scapegoat:** The character plants evidence pointing to someone else's responsibility for this action. The GM may decide this constitutes a Flashback scene to establish the character obtaining the evidence. The Investigation or Insight roll to see through this ruse gains a -2 penalty. This stunt costs 4 Degrees of Success.
*   **Shadow:** The character’s stealth is rewarded with an extra edge so long as they remain undetected. This may be due to being able to take extra time, or because they are attacking from ambush. Whatever reason is dramatically appropriate, gain a +2 on the character's next action by 2 so long as they remain unnoticed. This stunt costs 2 Degrees of Success.
*   **Tracking:** The character utilizes their knowledge of the outdoors to glean additional information about a character or creature that they are following. The character learns an additional fact about their quarry (i.e. they are wounded, they are wearing armor, they have an odd gait, etc). This stunt costs 2 Degrees of Success and can be taken multiple times.
*   **Treasure Hunter:** The character has incredible luck. While scouting, they find a small cache of loot, increasing their Treasure by 1. This stunt costs 4 Degrees of Success, can be chosen by the same character only once per episode, and is only available in games using Resources.


### Social Stunts

The following stunts primarily apply to social encounters, whether they are contests or conflicts. They may also apply to certain situations in exploration or other types of conflict, depending on the situation at hand.

| Social Encounter Stunts                                                                           |
|:---:|---------------------|-----------------------------------------------------------------------|
| DoS | Name                | Effect                                                                |
| 2   | Confidence          | Your Social RV is increased by 2 until your next turn                 |
| 2+  | Cutting Remark      | Your EV is increased by 1                                             |
| 2   | Flashy Introduction | Only for the first action of the scene, gain +2 against witnesses     |
| 2   | Innuendo            | Your words carry a different meaning for some listeners               |
| 2   | The Best Policy     | In response to your honesty, the target answers honestly              |
| 3   | Flirt               | Reduce the penalties of social rolls against an attracted target by 2 |
| 3   | Follow Up           | You take a second social action as part of the same Simple Action     |
| 3   | Jest/Inspire        | Allies get a free Morale or Focus recovery                            |
| 3   | Stunned Silence     | Enemies with fewer ranks of Presence take penalty on next action      |
| 4   | Piercing Wit        | Reduce the target’s Social RV by 2 for this attack                    |
| 4   | Vendetta            | Declare a character an enemy, +2 on physical conflict rolls against   |

*   **Confidence:** The success of the character’s interaction makes them feel an increased sense of self-assurance, increasing their Social RV by 2 until their next Turn. This stunt costs 2 Degrees of Success.
*   **Cutting Remark:** The character’s social attack is particularly vicious, increasing the EV for this attack by 1. This stunt costs 2 Degrees of Success and can be chosen multiple times.
*   **Flashy Introduction:** This character makes an extravagant entrance or an otherwise huge first impression. This stunt can only be taken on the first social action of a scene, and the character gains a +2 of the next action against any one target who witnessed it. This stunt costs 2 Degrees of Success.
*   **Flirt:** The character takes advantage of the attraction another target feels towards them. The gender of the target does not matter, so long as they feel a sexual/romantic attraction to the character (GM’s discretion). All social rolls this character makes against the target have their penalties reduced by 2 for the remainder of the encounter. The stunt costs 3 Degrees of Success.
*   **Follow Up:** Before the target can respond to their first social interaction, they perform another within the same Simple Action. This can allow a second attempt at a contest roll, or can actually lead to additional Morale damage for conflicts. This stunt costs 3 Degrees of Success.
*   **Innuendo:** The character is able to speak perfectly normal sounding sentences that carry an alternative meaning for others. The player chooses one target, enemy or ally, to have this second layer of understanding, and the GM may decide this requires a flashback to establish. This stunt costs 2 Degrees of Success.
*   **Jest/Inspire:** The character makes a joke to lighten the mood, or says just the right phrase to spur his allies on. In either case, allies make a free quick recovery on Focus or Morale, depending on which way the character goes. This stunt costs 3 Degrees of Success, and can only be chosen once per episode per character.
*   **Piercing Wit:** The character’s comments are particularly insightful, cutting through the target’s justifications. Reduce the target's Social RV by 2 for this attack. This stunt costs 4 Degrees of Success.
*   **Stunned Silence:** This action is so shocking, flamboyant, or just generally amazing that it leaves foes in awe. Any enemy with fewer ranks of Presence than this character gains a -2 penalty for any action taken for 1 turn. This stunt costs 3 Degrees of Success.
*   **The Best Policy:** By being completely honest or upfront, the character encourages the target to do the same on their next response. This does not prevent either side from omitting information, just from falsifying it. This stunt costs 2 Degrees of Success.
*   **Vendetta:** The character uses this interaction to also declare the target as an object of hatred. The next time the two characters meet, this character gains a +2 on their first attack against the target. This stunt costs 4 Degrees of Success and can only be chosen once per character per episode.


### Special Skill Stunts

This final group of stunts are focused on the crafting and creative skills, allowing a character to increase the quality or otherwise alter the things they are making.

| Special Skill Stunts                                                                   |
|:---:|-------------|--------------------------------------------------------------------|
| DoS | Name        | Effect                                                             |
| 2+  | Copycat     | This forgery is hard to distinguish from the real deal             |
| 2+  | Magnum Opus | Increase the Resource value of the created item                    |
| 3+  | Masterwork  | Increase the value as well as one of the stats of the created item |

*   **Copycat:** The character’s forgery is quite good, making it harder to detect. Increase the penalty on the Perception roll to discover the work of art is a forgery by 1. This stunt costs 2 Degrees of Success and can be chosen multiple times.
*   **Magnum Opus:** The character’s creation is particularly impressive, increasing the VR of a work of art by 1. This stunt costs 2 Degrees of Success and can be chosen multiple times.
*   **Masterwork:** The character’s creation is particularly impressive, increasing the VR of a crafted item by 1, as well as improving one other stat by 1. The possible attributes include (but aren’t limited to) the item’s base EV, RV, Hardness, bonuses to skills granted through its use. This stunt costs 3 Degrees of Success and can be chosen multiple times.


# NPCs

The final section is a quick overview of how to make NPCs in VERS. They could use the same rules as above, but that honestly gives way too much information for all but the most important NPCs. Instead, NPCs like companions, pets, and antagonists use an even more simplified system to flesh them out.

The first step in making an NPC is to determine if it is sentient or a beast. Don’t get too tripped up by the names, though. A clockwork automaton would be counted as a beast, for instance. The real difference is if the creature will have mental and social attributes, or if they will be negligible.

In either case, the NPC only has three attributes: either Mental, Physical, and Social for sentient creatures, or Power, Finesse, and Resistance for beasts. These otherwise act as normal, setting target numbers for skill rolls, but they are not purchased. They are set based on the desired power level and their place in the hierarchy.

---

If the GM wishes for more variability than these rules provide, these attributes can be ranked first, second, and third, with the first ranked attribute being one higher than these rules would normally create, and the third ranked attribute being ranked 1 lower.

---

The power level of the NPC should be the same as that of the PCs you are building them for. This sets the core number that all of the other parts will be derived from.

The other part is the NPCs hierarchy positions. This can be pawn, grunt, elite, and master. Most friendly NPCs will be of the first two categories, while fellow adventures, like pets or companions may be elites. Only the greatest of NPCs should be masters, creatures that will serve as major foils for the troupe.

Pawns have attributes, skills, and abilities equal to 2 less than their power level (for PL 2 and lower, skills and abilities can be present and simply not get a bonus). If they have gear, it will typically be of lesser quality (smaller bonuses, etc). These enemies only have a single condition slot used for any type of effect and of any net EV range. Because of their low stats, pawns are often used in groups of 3, and mostly just bulk up the opposing numbers without drastically increasing the difficulty.

Grunts are a little bit more powerful, having attributes, skills, and abilities equal to 1 less than their power level. They also typically have lower quality gear. They have two condition slots, one for net EVs from 0-4, and one for net EVs 5 and up. These slots are also universal, being used for any type of effect the character suffers.

Elites are roughly on par with the player characters, in that their attributes, skills, and abilities are equal to their power level. While they have the same universal condition slots, they have one at each level the player characters would normally have (0-2, 3-5, 6-8, and 9+).

Finally, masters have attributes, skills, and abilities 1 greater than their PL, and often have access to enhanced or special gear. They also use universal conditions, however they have the same number and same spread as players (2 slots 0-2, 2 slots 3-5. And one each 6-8 and 9+).


# Appendix A: Example Abilities

The following abilities are examples that can be easily dropped into games as they are, or they can serve as inspiration for new creations. Because of the loose nature of abilities, each section may define Properties or other pertinent information for the use of those abilities.


## Magic

These abilities would not be out of place in a game meant to feel a bit like some of the original generation of fantasy role-playing games, featuring high powered battles and magic that can level city blocks. In many systems like this the abilities are grouped into schools, although the inspiration is not any one version of that style of game, and so differ from  those described in those games.

A mage using these abilities is assumed to be defined as either Arcane or Divine (see the properties section for more information). Each of these is split into schools of magic, which can be specialized in. This takes the form of a 5 CP advantage called Specialist Magic, which grants a +1 to rolls and EVs with spells of that type and a similar penalty on a different school.

It's also important to note that this list tries to make as few assumptions about the way the setting may work as possible, but a few assumptions had to be made for any of it to work. First, there is an afterlife, and an assumption that not all dead get there, with some wondering the mortal realm as ghosts. It also assumes that there are elemental planes, which have some type of elemental spirits inhabit each of these planes. Finally, the assumption has been made that some type of “higher” planes exist, in which deities or other creatures exist, although these are not determined and left as higher spirits. Many aspects of divine magic are left for the world builder to define, like what forces are seen as allies and what forces are seen as foes to the religions in question.


### Properties

These properties are used in the following ability definitions: _Abjuration_, _Arcane_, _Conjuration_, _Curse,_ _Divination_, _Divine_, _Elemental_, _Illusion_, _Intervention_, _Mercy_, _Mysticism_, _Necromancy_, _Retribution_, _Ritual,_ _Sanctuary_, and _Transmutation_.

Abjuration denotes an arcane spell designed to protect the target, either from physical or magical harm. It also includes dispelling magic. A mage who specializes in abjuration is called an abjurer.

Arcane denotes any spell that draws on knowledge and will to work, as opposed to _Divine_, which draws on the intervention of gods or other greater beings. Characters who case arcane spells are generally called mages.

Conjuration denotes an arcane spell designed to draw or manipulate the denizens of other planes. Most frequently these are spirits (including demons, but not the dead) and elementals. This can also include manifesting objects out of thin air, or even calling animals. A mage who specializes in conjuration is called a conjurer.

Curse denotes a spell that is specifically used to permanently change or manipulate others against their will, physically, mentally, or emotionally. Only effects with the curse property can be removed with the _remove curse_ spell.

Divination denotes a divine spell that imparts information on the character in some way, often by enhancing the priest’s senses in some way. Priests that specialize in divination spells are call seekers.

Divine denotes any spell that draws upon faith and external power to work, such as gods, devils, or nature spirits. This is opposed to _Arcane_ which denotes spells based on learning and study of the way the world works. Characters who cast divine spells are generally known as priests.

Elemental denotes an arcane spell that manipulates one of the elements (air, earth, fire, water, or aether), which are treated as sub-properties. While the first four are familiar to most people, aether can be confusing. In essence it is energy, and encompasses light and lightning. Most elemental spells require the mage to specify the element of the spell when learning it (such as _elemental bolt_ becoming _fire bolt_, and gaining the Fire property). These spells can be learned multiple times, each with a different element, but must be improved separately. A mage who specializes in Elemental spells is called an elementalist.

Illusion denotes an arcane spell that alters the mind or at least the perceptions of the mind. This can be in the form of hallucinations, compulsions, or any other effect that is purely mental or emotional in nature. A mage who specializes in Illusion is called an illusionist.

Intervention denotes a divine spell that calls on the god or spirit to intervene in the world directly, in ways that are not part of other divine schools. A priest who specializes in intervention spells is called an arbiter.

Mercy denotes a divine spell that focuses on healing and curing, allowing the priest to mend wounds, remove disease, neutralize poison, and even remove curses. A priest who specializes in mercy spells is called a healer.

Mysticism denotes an arcane spell that deals specifically with the more esoteric spells, such as those that allow the mage to move items without touching them, move great distances without moving, or manipulate knowledge itself. A mage who specializes in mysticism is called a mystic.

Necromancy denotes an arcane spell that deals specifically with the dead, dying, or decay. These type of effects are rare, and could be argued to more appropriately belong to other schools, however, because of the negative light necromancy is viewed in by most societies, this school is mostly used to gate keep these effects. A mage who specializes in necromancy is called a necromancer.

Retribution denotes a divine spell that invokes the wrath of their deity or the spirits, damaging or destroying foes of the faith. It is important to note here that each religion is going to denote its enemies differently, and the form of these attacks can vary, based on the nature of the deity. Priests who specialize in retribution spells are called scourges.

Ritual denotes a spell that takes time to complete, instead of being able to be cast immediately, like most. Ritual spells usually involve special incantations and gestures, and may require special glyphs, tools, or even a special location. A ritual takes a number of minutes to cast equal to 10 times the cost.

Sanctuary denotes a spell that protects the mage and/or their allies. This may be from specific sources, or general, and could apply to only one target or many. A priest who specializes in sanctuary spells is called a defender.

Transmutation denotes an arcane spell designed to change or transform the target, be it the form or the function. This can include effects that transform properties of nature, but not changes to mental or emotional states. A mage who specializes in transmutation is called a transmuter.


### Arcane Magic

These characters use their great learning coupled with their force of will to impose their desires on the fabric of reality. This is represented by the new skill, _Arcana_, based on Logic. An Arcana roll is used to cast spells, either written in their spellbooks, upon scrolls, or memorized. The EVs for these abilities come from the ranks in the abilities themselves.

For higher powered games, the GM may determine that the character’s Logic is also added in to determine the full EV. For lower powered games, such as sword and sorcery style games, casting spells also, in addition to the Focus condition for sustained abilities, gives the character a Injury condition, to show the physically demanding nature of magic. This condition is based on the EV of the spell, and uses Stamina as the RV.

To learn a new spell the character needs to have access to an NPC that can teach them (often for a cost, in favors or coin), or they can learn it from a scroll or spellbook on their own with a number of weeks of study equal to the cost per rank.


#### Spell List

**Animate Dead:** The mage calls forth a spirit and forcibly inserts it into a body, bringing it back to some semblance of life. The trauma is often enough to leave the newly animated dazed and stumbling. The animated dead is not immediately under control, and may turn on the animator.

*   **Properties:** Arcane, Necromancy, Ritual
*   **Manifestation:** Spoken Words, Gestures, Glyphs
*   **Duration:** Instant
*   **Effect:** EV determines success. EV equal to PL of reanimated creature creates a pawn level creature, EV equal to twice the PL creates a grunt, etc. A previously summoned spirit can be used this way without doing the ritual.
*   **Cost:** 2 CP

**Animate Object:** The mage calls forth a spirit and forcibly inserts it into an object, giving a semblance of life. If this object is in the shape of a creature (such as a statue, etc) then it moves as that creature would. However, if the object has a more mundane shape then it moves by floating. The animated object is not immediately under the control of the mage that animated it, and may turn on them.

*   **Properties:** Arcane, Conjuration, Ritual
*   **Manifestation:** Spoken Words, Gestures, Glyphs
*   **Duration:** Instant
*   **Effect:** The same as _animate dead_.
*   **Cost:** 2 CP

**Anti-Magic Shell:** The mage creates an area around them that ends other spell effects and prevents new ones from being cast or activated.

*   **Properties:** Arcane, Abjuration
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** EV reduces the EV of existing effects and acts as a RV against new effects within the current zone.
*   **Cost:** 4 CP

**Charm:** The mage creates an aura that makes themselves seem more appealing, reasonable, or charismatic. This is accomplished by lowering the target’s inhibitions and filtering their perceptions.

*   **Properties:** Arcane, Illusion, Curse
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** Net EV/2 as a penalty to Discipline or any other rolls to resist social advances by the user, whether they are attacks or just contests.
*   **Cost:** 3 CP

**Confusion:** The mage alters the perceptions of the target, making it difficult to separate reality from fiction. This can lead the character to behave erratically.

*   **Properties:** Arcane, Illusion, Curse
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** Focus attack, with GM deciding additional delusions based on severity of Focus condition.
*   **Cost:** 3 CP

**Darkness:** The mage manipulates the perceptions of foes in a zone, blinding them with an illusion of shadowy mists.

*   **Properties:** Arcane, Illusion
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** Penalty equal to net EV/2 on vision based Perception for all foes within a zone.
*   **Cost:** 4 CP

**Darkvision:** The mage changes the biological nature of the target’s eye, granting them the ability to see in the dark like a cat.

*   **Properties:** Arcane, Transmutation
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** EV/2 as a bonus on vision based Perception rolls, but only to counteract penalties from natural darkness. This means that this can never give more than a net +0 to the roll overall, if half the EV is higher than the penalties involved.
*   **Cost:** 2 CP

**Detect Magic:** The mage is able to sense that magical effects are active, or were recently active in the area. Extremely skilled mages can determine the type of magic involved, the school, and sometimes even the spell itself and who cast it. This does not provide information about the actual location of the effect or the caster.

*   **Properties:** Arcane, Mysticism
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** Perception is within the zone currently occupied, but sustain spell will update as the character moves through zones. EV determines the amount of information. Simple level effects can determine Arcane, Divine, or if the effect was an Enchanted item. Minor can determine the school, Major can determine the spell, and Extreme can determine gender and species of caster.
*   **Cost:** 3 CP

**Dimension Door:** The mage opens a portal to another place, often on another plane, but it could be on the same plane. This portal remains open as long as the mage maintains it, and any number of creatures can pass through in either direction while it is open.

*   **Properties:** Arcane, Mysticism
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** If the mage is physically familiar with the place the portal leads, then a simple success is all that is required. If the mage is not, then EV determines how close they get to where they want to go.
*   **Cost:** 4 CP

**Dispel Magic:** The mage cancels the targeted sustained effect, at least if their power is greater than that of the one who cast the original spell.

*   **Properties:** Arcane, Abjuration
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** If EVs are greater than EVs of targeted effect, it is dispelled.
*   **Cost:** 3 CP

**Drain Life:** The mage absorbs the life energy of a target and uses it to heal themselves. This is not perfectly efficient, however, healing the mage less than the damage dealt.

*   **Properties:** Arcane, Necromancy
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** Treat as normal physical damage, however the attacker instantly heals an injury one level lower than the one dealt, or a lower one if they have no injuries at that level.
*   **Cost:** 4 CP

**Elemental Bolt:** The mage unleashes a ranged attack on a single target based on the chosen element: sand or stone for earth, fire, water or ice, wind, or light or lightning for aether.

*   **Properties:** Arcane, Elemental
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV is damage, plus any logical incidental effects of subtype
*   **Cost:** 3 CP

**Elemental Strike:** The mage unleashes an explosive elemental attack striking multiple enemies in a single zone. The element for this spell must be chosen when learned, like Elemental Bolt.

*   **Properties:** Arcane, Elemental
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV is damage, effecting 3 foes and any engaged with them, plus an additional foe for every 3 EV. All affected characters must be in the same zone.
*   **Cost:** 4 CP

**Elemental Wall:** The mage calls elemental energies to create a wall. This wall can be made in any configuration, and is at least 1 m high and 1 m long. Depending on the element in question, this wall may be moved through, however those who do are affected by elemental damage equal to the EV.

*   **Properties:** Arcane, Elemental
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** EV sets the hardness on solid walls (stone, ice) while it is elemental damage on passable walls. EV also sets the length and the height of the wall, length is equal in meters to the EV, while the height is 1 plus EV/3 meters.
*   **Cost:** 4 CP

**Feeblemind:** The mage saps the mental acumen of the target, causing the brain function to decay.

*   **Properties:** Arcane, Necromancy, Curse
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** Logic, Intuition, and Resolve are all reduced by EV/3. This also affects Dodge and Discipline.
*   **Cost:** 5 CP

**Flesh to Element:** The mage transforms and traps a creature into an elemental form, most typically stone, but could be ice, an everlasting flame, or a sentient, immobile cloud, or others.

*   **Properties:** Arcane, Elemental
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV gives generic condition. Only succeeds if it achieves an Extreme effect. Resultant form is alive and aware, but cannot communicate or move.
*   **Cost:** 2 CP

**Hex:** The mage curses the target with grave misfortune until the curse is lifted.

*   **Properties:** Arcane, Necromancy, Curse
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** Target takes a penalty equal to EV/3 on all rolls made until the curse is lifted.
*   **Cost:** 4 CP

**Invisibility:** The mage alters the perceptions of all foes in the area, making them forget the presence of the target. This forgetfulness will not hide the target in all cases, however, and some actions, like attacks, will be noticed, breaking the spell.

*   **Properties:** Arcane, Illusion
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** Target gets a bonus to Stealth rolls equal to EV/2. The actions that will break the spell vary with EV level. Simple level can be broken by simply moving, Moderate by interacting with objects, Major by interacting with a creature, and Extreme only by attacking.
*   **Cost:** 2 CP

**Open/Seal:** The mage is able to unlock any mundane lock, remove any magical seal, lock a mundane lock without the key,  or seal any portal or container with a magical seal.

*   **Properties:** Arcane, Transmutation
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV is compared to lock level to lock or unlock, while EV of a magical seal must be overcome by the EV of this spell to be opened.
*   **Cost:** 3 CP

**Polymorph Target:** The mage transforms the target into a different creature. The character can choose the new form, or the GM can choose it randomly, however the new form must be a living creature, and cannot be a plant or mineral.

*   **Properties:** Arcane, Transmutation, Curse
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV gives general condition to determine the new form’s difference. Simple would be a change in coloration or size, Moderate would be a different related species or gender, Major would be a different species, but still mammal, or reptile, etc. Extreme would be any type of creature otherwise.
*   **Cost:** 3 CP

**Read Languages:** The mage allows the target to read any written language, even those that are not otherwise known or understood, such as lost languages or those of alien design. This does not give them the ability to write in this language or to speak it.

*   **Properties:** Arcane, Mysticism
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** A simple success is that is required.
*   **Cost:** 3 CP

**Remove Curse:** The mage undoes the harmful magic affecting the target, breaking any existing curse on them.

*   **Properties:** Arcane, Abjuration
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** Compare the EV of this spell to the EV of the curse. If this EV is higher, the curse is broken.
*   **Cost:** 3 CP

**Shield:** The mage creates a physical barrier that protects them from harm as well as allies within the same zone.

*   **Properties:** Arcane, Abjuration
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** EV sets the Hardness of the shield. Allies within the zone and any creatures they are engaged in can also be enclosed in the shield at the mage’s desire, however the effective harness is halved.
*   **Cost:** 4 CP

**Sleep:** The mage affects the mind of the target, forcing them into slumber, even while standing. These slumbering creatures are able to be awakened as any normal creature, however and are not kept asleep.

*   **Properties:** Arcane, Illusion
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV gives a general condition. A creature unaware of danger who is tired already only requires a Moderate level, while unaware in the middle of the day would be a Major level. If the target is aware of danger, then an Extreme level is required.
*   **Cost:** 3 CP

**Summon Dead:** The mage calls up the soul of a recently dead creature, or with more effort, a soul long dead and departed from the mortal plane. This spirit is not automatically friendly to the mage that summons them and may turn on them.

*   **Properties:** Arcane, Necromancy, Ritual
*   **Manifestation:** Spoken Words, Gestures, Glyphs
*   **Duration:** Instant
*   **Effect:** EV/2 is the maximum PL of a recently departed soul that can be called, while EV/3 is the maximum PL of a long dead soul that can be called. 
*   **Cost:** 2 CP

**Summon Elemental:** The mage calls a denizen of one of the elemental planes, causing the elemental to appear before them. An elemental so summoned is not automatically friendly toward the summoner.

*   **Properties:** Arcane, Conjuration, Ritual
*   **Manifestation:** Spoken Words, Gestures, Glyphs
*   **Duration:** Instant
*   **Effect:** EV/2 is the maximum PL of elemental that can be called.
*   **Cost:** 2 CP

**Summon Spirit:** The mage calls a being from one of the higher plains, often referred to as angels, demons, or less commonly nature spirits. The spirit is not automatically friendly toward the summoner.

*   **Properties:** Arcane, Conjuration, Ritual
*   **Manifestation:** Spoken Words, Gestures, Glyphs
*   **Duration:** Instant
*   **Effect:** EV/2 is the maximum PL able to be called for a nature spirit, while one of the higher spirits is EV/3 for the maximum PL called.
*   **Cost:** 2 CP

**Summon Wisp of Light:** The mage calls an aether or fire elemental of the most basic power to provide light. This elemental will follow basic instructions (go over there, come here, etc) but will not communicate in any way, nor will it fight for the calling mage.

*   **Properties:** Arcane, Conjuration
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** EV/2 determines the brightness of the light provided.
*   **Cost:** 1 CP

**Telekinesis:** The mage gains the ability to affect objects without touching them. They must balance the strength of this ability with the distance over which they are able to act. 

*   **Properties:** Arcane, Mysticism
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Sustained
*   **Effect:** For 2 ranks of EV, the effect gains 1 Strength, for 2 ranks of EV, the effect can reach an additional zone away. The player determines how they want to use the EV each time they use the ability.
*   **Cost:** 3 CP

**Teleport:** The mage moves instantly to another location they have physically been to before. They are not always able to carry anyone else with them, but they do always bring all of their gear.

*   **Properties:** Arcane, Mysticism
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV/3 to include other creatures, who must have skin contact with the mage to be brought along.
*   **Cost:** 4 CP

**Trap:** The mage places a magical trap in a zone, which goes off when that zone is entered by anyone not considered an ally of the mage. This trap can take several sub forms, and one must be chosen by the mage when learning this spell. These include paralyzing the enemy, catching them in webs, greasing the floor with flammable oil, or simply creating an ear-splitting noise.

*   **Properties:** Arcane, Abjuration, Ritual
*   **Manifestation:** Spoken Words, Gestures
*   **Duration:** Instant
*   **Effect:** EV/2 sets the required Athletics roll to get out of paralysis or restraining webs, the penalty to Athletics rolls to remain standing with the oil, or the Focus damage taken by all enemies in the zone for the noise.
*   **Cost:** 2 CP


### Divine Magic

These characters channel the divine power of deities, devils, or other great forces. This is represented by the new skill, _Faith_, based on Intuition. A Faith roll is used to access divine magic, focused through a holy symbol (or maybe a tattoo or something similar), which the character must hold in their hand or otherwise display obviously while casting. The EV of the spell is based only on the ranks in the ability itself.

For higher powered games, the GM may determine that the character’s Logic is also added in to determine the full EV. For lower powered games, such as sword and sorcery style games, casting spells also, in addition to the Focus condition for sustained abilities, gives the character a Injury condition, to show the physically demanding nature of magic. This condition is based on the EV of the spell, and uses Stamina as the RV.

To learn a new spell the character needs to have access to an NPC that can teach them (often for a cost, in favors or coin), or they can learn it from a scroll or spellbook on their own with a number of weeks of study equal to the cost per rank.


#### Miracle List

**Awaken:** The priest channels the divine energies to give sentience to an object or creature, raising them to the level of sentient creature. Unlike similar arcane spells, these beings are initially friendly, although they can turn on the priest if mistreated. 

*   **Properties:** Divine, Intervention, Ritual
*   **Manifestation:** Spoken Words, Focus, Glyphs
*   **Duration:** Instant
*   **Effect:** EV/3 is the PL of the awakened being. The being knows the language of the priest that awakened it, however it probably cannot speak it based on biology (GM discretion).
*   **Cost:** 3 CP

**Banish:** The priest forces oppositely aligned spirits in the current zone out of the mortal plane. These spirits are returned to their proper planes immediately, and any object or creature that had been possessed by them is returned to normal, although possibly weakened by the ordeal as fit by the story.

*   **Properties:** Divine, Sanctuary
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** EV/3 sets the upper limit of PL of spirit that can be banished in this way, while any others take damage equal to the EV.
*   **Cost:** 4 CP

**Beacon of Faith:** The priest channels divine energy into their bodies, shedding light that eliminates darkness within the current zone. Zones beyond have a -2 penalty per zone. This light is reflective of the divine power invoked, for instance a golden light for a sun deity, while a smith deity may shed a fiery glow of a forge.

*   **Properties:** Divine, Intervention
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** Simple success is all that is needed for mundane darkness, while supernatural darkness has its EV compared to the EV of the priest.
*   **Cost:** 4 CP

**Bless/Curse:** The priest channels divine energy into the area they are in or into their allies or foes. This energy gives the creatures or place a bonus (or penalty). Allies can pick when to use the bonus, while foes take the penalty on their next roll. The priest determines whether the target is the area or the people in it when casting.

*   **Properties:** Divine, Intervention, (Curse)
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** Allies get a bonus equal to EV/2 that they can apply to any one roll before the end of the scene. An area gets a roleplaying or story related bonus for an area equal to EV/3 km in radius. This could be better crops in the next harvest, protection from disease for a year, or something similar.
*   **Cost:** 4 CP

**Control Weather:** The priest channels the divine powers to change the weather in an area, based on the strength of the effect.

*   **Properties:** Divine, Intervention
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** Affects an area EV/3 miles in radius, while the degree of change in the weather is based on the general condition level, with simple being and increase of decrease in one aspect of the weather (more cloudy, less cloudy, more rain, less rain, etc). On the other end, extreme changes can turn a heat wave into a blizzard. The weather continues as would be natural, usually ending within a few hours.
*   **Cost:** 5 CP

**Cure/Give Disease:** The priest channels the divine power of their deity to remove a disease (or bestow one, depending on the deity/force).

*   **Properties:** Divine, Mercy, (Curse)
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** Effect removed (or given) based on the EV,  removing (or giving) an affliction of that the general condition. Simple would be nausea or other minor illness, Moderate would be cold or flu, Major would be arthritis or other chronic but rarely terminal disease, and Extreme would be cancer or AIDS.
*   **Cost:** 3 CP

**Cure/Inflict Wounds:** The priest channels the power of their deity, healing injuries (or creating them, based on the deity).

*   **Properties:** Divine, Mercy
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** EV is a bonus on the target’s next recovery action, which they are able to make immediately (for lower powered games, the character must wait until their normal recovery action). To inflict wounds, the EV is treated as damage.
*   **Cost:** 3 CP

**Death Ward:** The priest channels the divine power of their deity to preemptively protect a creature from a deathblow, holding them at the edge of death if they are grievously injured but preventing them from being slain. This allows the priest or another character to come to their rescue. There is no outward sign that the protected creature was saved.

*   **Properties:** Divine, Sanctuary
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** A simple success is all that is needed, any additional damage is ignored up to the EV of the effect. Anything over that amount still damages the character and breaks the spell, killing them.
*   **Cost:** 2 CP

**Detect Foe:** The priest opens their senses to the power of their deity, allowing them to sense creatures, items, and even places that are powered by their deities opposing forces.

*   **Properties:** Divine, Divination
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** Bonus to Perception rolls equal to EV/2, only to sense the presence of creatures, items, or places that oppose their deity.
*   **Cost:** 3 CP

**Dispel Opposed Effect:** The priest channels their deity’s power and removes spell effects created by priests or mages channeling opposed forces.

*   **Properties:** Divine, Protection
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** The EV of this ability is compared to qualifying sustained or inherent effect (GM discretion) and if it is greater, it removes it.
*   **Cost:** 2 CP

**Cataclysm:** The priest channels their deity’s power to create a localized cataclysm, damaging structures and injuring people in that area. The nature of this cataclysm is based on the nature of the deity, but could be lightning strikes or a tornado for a storm deity, a localized earthquake for an earth deity, or maybe rapid decay of structures for a death deity. 

*   **Properties:** Divine, Retribution
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** Area affected is EV x 10 m in radius, the damage is equal to the EV to any creatures in the area, twice that amount to structures and items.
*   **Cost:** 5 CP

**Find Path:** The priest opens up their senses to their deity, receiving a sense of the general direction toward a person, place, or thing sought by the priest.

*   **Properties:** Divine, Divination
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** General success is all that is needed, unless being hidden or shielded. EV is used to determine if sense can penetrate the hiding effect.
*   **Cost:** 3 CP

**Hand of Protection:** The priest channels divine energy and protects themselves and all allies within the same zone against physical damage. This may take on various forms depending on the nature of the deity.

*   **Properties:** Divine, Sanctuary
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** EV/2 is added to the RV of all allies within the current zone, or within the zone in which the effect is centered if not on the priest.
*   **Cost:** 4 CP

**Hold Creature:** The priest uses their connection to divinity as a conduit, channeling divine energy that envelopes all nearby foes and prevents them from moving or acting. This effect may vary slightly based on the deity in question, like manifesting as chains for a smith deity, while a deity of weather or cold may freeze them in blocks of ice.

*   **Properties:** Divine, Intervention
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** EV/2 is the penalty on any Athletics or Contortion rolls to escape. This affects all foes in the targeted zone.
*   **Cost:** 4 CP

**Holy Word:** The priest focus the will of their deity into a thunderous shout, calling out a holy word, sacred to their faith, which causes opposed creatures within hearing to cower in fear.

*   **Properties:** Divine, Retribution
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** EV is Morale damage to all opposed creatures within the same zone as the priest. This spell does not differentiate between allies and enemies, all those doctrinally opposed to the priest’s sect are damaged.
*   **Cost:** 3 CP

**Judgement:** The priest channels divine energy into their strike, damaging doctrinally opposed creatures with their touch. This is often used in addition to or delivered by an attack, and can be channeled through the priest’s weapon. This may take on different manifestations based on the deity, such as flames, frost, thorns, or whatever else makes sense to the religion in question.

*   **Properties:** Divine, Retribution
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** EV/2 added to the standard damage of the attack.
*   **Cost:** 3 CP

**Neutralize/Inject Poison:** The priest channels their divine energies and purges (or injects) the target’s system of any poisons or other dangerous chemicals, immediately ending their effect, but not healing any damage that had already been done.

*   **Properties:** Divine, Mercy
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** EV negates the poison based on the general condition, while the EV afflicts the target based on the general condition achieved.
*   **Cost:** 2 CP

**Projection:** The priest frees their soul from their body, leaving it in a vegatative state while the soul travels. Their spirit form is connected only by a faint cord connecting their physical navel to their spiritual one. While traveling in the shadow realm, they are able to see and hear the mortal plane, but cannot touch or otherwise sense it, nor can they be sensed by those in the mortal realm. If their umbilical line is cut or otherwise severed, their physical body immediately dies, and they become a wandering ghost. There is no limit to the distance traveled this way, which is far faster than ordinary travel, including traveling to other planes. The character’s form in the shadow is a reflection of their view of themself, and anything that would normally require physical abilities are instead based on mental ones.

*   **Properties:** Divine, Divination, Ritual
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** General success is all that is needed, the EV determines the Hardness of the spiritual umbilical, as well as the strength of their “weapons” and “armor” if engaging in combat, each equal to EV/2.
*   **Cost:** 2 CP

**Protection from Foe:** The priest gathers their divine powers and makes a zone impenetrable to creatures and abilities opposed by their doctrine.

*   **Properties:** Divine, Sanctuary
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** EV is used as RV against effects targeting those in the protected area, while EV/2 is the PL of creatures kept out.
*   **Cost:** 3 CP

**Purify Food and Drink:** The priest uses their divine connection and makes food edible and water potable within the zone they occupy. This does not change or improve the flavor of food or water so affected, however. Any water being purified must either be contained in some way (barrels, cups, water skins, etc) or be completely within the zone in question (a puddle, etc). This has no effect on rivers, streams, or sections of lakes, ponds, or seas that may fall within the zone.

*   **Properties:** Divine, Mercy
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** EV to dispel any negative effect on food or drink Otherwise, to change food or drink which is non-consumable for any mundane reasons (rotten, polluted), treat the EV as a transformation and base the amount of change on the general condition. 
*   **Cost:** 3 CP

**Quest:** The priest sets a task for a creature capable of hearing and understanding their words. This creature then is compelled to take up the proscribed  action, and will until it is completed. This is often used to redeem those that have sinned according to the church or have broken laws in secular society. The task cannot be truly harmful to the questor, but it may involve lessons of humility or altruism that put the questor in discomfort of some kind. This is still considered a curse, however.

*   **Properties:** Divine, Intervention, Curse
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** Player must decide the desired task, the GM sets the general condition level, and the compulsion is only given if the net EV is sufficient.
*   **Cost:** 3 CP

**Remove Curse:** The priest cancels any negative long term effects on the character. Other than the origin of the magic and the properties and manifestations, this works as the arcane variant.

*   **Properties:** Divine, Mercy
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Instant
*   **Effect:** See the [arcane variant](#bookmark=id.1bwbjec8e8vi).
*   **Cost:** 3 CP

**Silence:** The priest exerts their divine power and strikes dumb all foes within a zone, garbling, masking, or completely removing their ability to communicate, verbally or by sign language..

*   **Properties:** Divine, Retribution
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** EV is used to give a general condition to all in a zone, based on the RV of the most powerful in the group. The Simple level converts the words to a different but common language (comprehendable with a Linguistics roll or Language advantage). The Moderate level reverses or otherwise obscures the words in addition to altering the language (requires Linguistics roll with a -4 penalty, in addition to the appropriate Language advantage). The Major level renders it completely unintelligible (no longer language at all). Extreme level disallows even attempts to communicate, at most grunting. 
*   **Cost:** 4 CP

**Speak Tongue:** The priest draws upon the sacred knowledge of the heavens and gains the ability to speak and understand all languages, although they cannot read or write them. If the deity in question is a nature oriented deity, this may include (or only work at all) on beasts, GM discretion based on the setting.

*   **Properties:** Divine, Divination
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** A simple success is all that is needed.
*   **Cost:** 2 CP

**Speak with Dead:** The priest uses their divine abilities to open a channel to the underworld. This allows the priest to question the deceased without profaning the world of the living by bringing them back physically. It is still frowned on by some deities, however, who forbid any contact with the dead by the living.

*   **Properties:** Divine, Divination
*   **Manifestation:** Spoken Words, Focus
*   **Duration:** Sustained
*   **Effect:** The player may ask EV/3 questions of the spirit, which the GM will answer _to the best knowledge of the dead spirit_. This means the information may be incorrect, misremembered, or otherwise unreliable, however the nature of the spell prevents outright falsehood. The spirit _believes_ the information given to be 100% accurate.
*   **Cost:** 3 CP

**Summon Spirit:** The priest calls upon their deity to send them a servant or messenger. The spirit called is _exclusively_ of the type devoted to the priest’s deity, and is initially friendly. Otherwise it works exactly like the arcane variant.

*   **Properties:** Divine, Divination
*   **Manifestation:** Spoken Words, Focus, Ritual
*   **Duration:** Instant
*   **Effect:** See the [arcane variant](#bookmark=id.dluyhpdr821h).
*   **Cost:** 2 CP


## Psionics

Psionics are powers of the mind, many of which are attested in the real world throughout history (although none have yet been proven by science to exist), and are often found in genres like horror and settings in the modern day or sci-fi. These are split into a few sub-categories based on types of effect, as discussed below in the properties section.

To use psionics, treat the individual abilities as a skill based on Intuition. This means that the activation roll (which also substitutes for any attack roll needed) is 3d6 + the ranks of the ability, and the roll result is compared to the Intuition TN. The EV is equal to the ranks in the ability plus their ranks in Logic. Those being targeted by psionics get their Discipline as their RV.

Also, it is important to note that these abilities sometimes have prerequisites. This means the character needs to have other skills or abilities before they can learn the ability with the prerequisite. This only has to be a single rank to qualify unless determined otherwise by the GM.

Most psionic abilities have a manifestation of “mental pressure.” What this means varies from person to person, but is often a feeling of vertigo or lightheadedness, although an indescribable scent, a movement in the corner of your eye, or even the metallic taste of aluminum foil on your teeth and many others are certainly possible. The important thing to note is that once the character has experienced psionic abilities a few times they can learn to notice these tells and prepare themselves.


### Properties

These properties are used in the following ability definitions: _ESP_, _Medium_, _Psionics_, _Psychokinesis_, and _Telepathy_.

ESP stands for Extra Sensory Perception, and these abilities are those related to the use of psychic power to extend the senses, allowing the user to perceive things they would not otherwise be able, whether because of distance or because other facets of their nature make them normally imperceptible to humans (like dreams). Characters who specialize in ESP abilities are often called clairvoyants.

Medium abilities are those related ghosts and spirits, and range from contacting and communicating with them to actually channeling them. Characters who specialize in medium abilities are, unsurprisingly, called mediums.

Psionics is a general term for any special ability the character may have that stems or is controlled by the mind. Characters who have psionic abilities are generally called psychics.

Psychokinesis abilities, often simply shortened to PK, involves the character directly interacting with the physical world using only their mind. This may involve moving items, changing temperatures, or other similar effects. Characters who specialize in psychokinesis are called psychokinetics.

Telepathy abilities are the ability to touch the minds of other humans, establishing mind to mind communication, and reading thoughts. Characters who specialize in telepathy abilities are called telepaths.


### Psionic Abilities

**Animal Empathy:** The character uses this ability to Intuit animal thoughts and moods. While animals do not share human level cognition, they do have emotions and basic thoughts that can be deciphered.

*   **Prerequisites:** —
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure, Attention to animal 
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:**  

**Animal Possession:** Control animal's body

*   **Prerequisites:** Animal Empathy
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Animal Rapport:** Mental bond with single animal

*   **Prerequisites:** —
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Inherent
*   **Effect:** 
*   **Cost:**  

**Astral Projection:** Soul travel by leaving body

*   **Prerequisites:** Clairvoyance
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Aura Reading:** See auras

*   **Prerequisites:** —
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Automatic Writing:** Trance state confers symbolic answers

*   **Prerequisites:** Channeling
*   **Properties:** Psionic, Medium
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Biokinesis:** Acquire temporary merits with short durations

*   **Prerequisites:** —
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Channeling:** Gain skills from unconscious, past lives, or ghosts

*   **Prerequisites:** Ghost Calling
*   **Properties:** Psionic, Medium
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Clairvoyance:** Remote viewing

*   **Prerequisites:** —
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Death Sight:** See and interact with ghosts

*   **Prerequisites:** —
*   **Properties:** Psionic, Medium
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Dowsing:** Ability to find hidden things

*   **Prerequisites:** Clairvoyance, Channeling
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:**
*   **Cost:** 

**Dream Travel:** Visit other's dreams

*   **Prerequisites:** Astral Projection, Mind Reading, Thought Projection
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Ghost Calling:** Summon ghosts to materialize

*   **Prerequisites:** —
*   **Properties:** Psionic, Medium
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Mental Blast:** Telepathic mental attack

*   **Prerequisites:** Thought Projection
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:**  

**Mind Breaker:** Inflict derangements

*   **Prerequisites:** Thought Projection
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** 
*   **Effect:** 
*   **Cost:**  

**Mind Control:** Issue commands

*   **Prerequisites:** —
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Mind Reading:** Hear and understand other's thoughts

*   **Prerequisites:** —
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Plant Empathy:** Lets PC's speed the growth of plants

*   **Prerequisites:** Biokinesis
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Postcognition:** View the past events of a location

*   **Prerequisites:** Precognition
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Precognition:** View/Predict the future

*   **Prerequisites:** —
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Psychic Empathy:** Adjust other's moods and auras

*   **Prerequisites:** Aura Reading, Thought Projection
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:**  

**Psychic Healing:** Ability to heal self or others

*   **Prerequisites:** Biokinesis
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Psychic Illusions:** Create illusions for others

*   **Prerequisites:** Mind Control, Thought Projection
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Psychic Invisibility:** Mask yourself from sight, not electronics

*   **Prerequisites:** Mind Control, Thought Projection
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Psychic Vampirism:** Ability to drain others of their willpower

*   **Prerequisites:** Biokinesis
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Psychometry:** Sense the history of objects

*   **Prerequisites:** —
*   **Properties:** Psionic, ESP
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Pyrokinesis:** Ability to make things spontaneously combust

*   **Prerequisites:** Pyrokinetic Shaping
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:** 

**Pyrokinetic Immunity:** Makes PC highly resistant or immune to fire damage

*   **Prerequisites:** Pyrokinesis
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Inherent
*   **Effect:** 
*   **Cost:** 

**Pyrokinetic Shaping:** Can manipulate the growth and direction of existing fires

*   **Prerequisites:** —
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Shielding:** protect against other psionic abilities

*   **Prerequisites:** —
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:** 

**Telekinesis:** Ability to move objects by the power of the mind alone

*   **Prerequisites:** —
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Telepathic Communication:** Initiate two-way telepathic conversation

*   **Prerequisites:** Thought Projection
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Sustained
*   **Effect:** 
*   **Cost:**  

**Telepathic Rapport:** Permanent telepathic communication link

*   **Prerequisites:** Telepathic Communication
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Inherent
*   **Effect:** 
*   **Cost:** 

**Thermokinesis:** Ability to raise or lower ambient temperature in an area

*   **Prerequisites:** —
*   **Properties:** Psionic, Psychokinesis
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:**   

**Thought Projection:** Send one-way thoughts to another

*   **Prerequisites:** Mind Reading
*   **Properties:** Psionic, Telepathy
*   **Manifestation:** Mental pressure,
*   **Duration:** Instant
*   **Effect:** 
*   **Cost:**  


## Superpowers

Superpowers are hard. There are so many different types, permutations, and flavors, such as teen heroes, street-level vigilantes, and galactic powered heroes, that it would be simply impossible to provide anything really definitive here. So instead this will be a short sample list of some famous powers with the serial numbers rubbed off, purely to serve as examples to guide you.


# Appendix B: Example Gear

The following equipment is roughly arranged from most ancient to most futuristic. Equipment like bows and swords will not be repeated as all equipment in this document are being created with all time frames in mind. What this means is that it would be possible to take a weapon from the ancient list and use it in a space opera setting, perhaps as a museum piece or even as belonging to a primitive species.

 That said, the Value represents a setting/genre in which the weapon is still in use or able to be obtained (reenactors, historian smiths, etc). Also, note that the gear shown here is as “Simulationist” (in effects) as VERS can get, just as an example, however, it does not try to differentiate between various underlying subtypes of weapons and armor, instead trying to leave those details to aesthetics. As always, this list is not intended to be definitive, and represents examples of how to build equipment for your game and setting.


## Prehistoric Gear

The following equipment would be suitable for a game set in the most ancient of human times, such as a neolithic or early bronze age game.

|                                                       Prehistoric Gear                                                           |
|-------------------------------|:--:|:----:|:-----:|:----:|:----:|---------------------------------------------------|:--:|:-----:|
|                                                         Close Combat                                                             |
| Name                          | EV | Size | Reach | Mass | Type | Notes                                             | GP | Value |
| Dagger                        | +1 | -3   | -1    | 0    | P,S  | Aerodynamic, +1 to Parry attempts                 | 10 | 5     |
| Club                          | +1 | -2   | 0     | 0    | B    |                                                   | 4  | 1     |
| War Club                      | +2 | -1   | 0     | 1    | B    | 2-Hand                                            | 2  | 2     |
| Hand Axe                      | +2 | -3   | 0     | 0    | B,P  | Aerodynamic, Massive Damage, -1 to Parry          | 14 | 3     |
| Spear                         | +3 | 1    | +1    | 0    | P    | 1.5 Hands, Set                                    | 6  | 2     |
|                                                        Ranged Combat                                                             |
| Name                          | EV | Size | Cap   | Mass | Type | Notes                                             | GP | Value |
| Stone                         | +1 | -5   | -     | 0    | B    |                                                   | 7  | -     |
| Javelin/Short Spear           | +2 | -2   | -     | 0    | P    | Aerodynamic                                       | 9  | 2     |
| Sling                         | -  | -5   | -     | 0    | -    | +2 Athletics to throw stones                      | 7  | 1     |
| Atlatl                        | -  | -3   | -     | 0    | -    | +2 Athletics to throw javelins                    | 5  | 1     |
| Short Bow                     | +2 | -2   | 4     | Spc  | P    | 2-Hands, Requires Athletics 2                     | 3  | 2     |
| Long Bow                      | +3 | -1   | 4     | Spc  | P    | 2-Hands, Not usable mounted, Requires Athletics 2 | 1  | 3     |
|                                                            Armor                                                                 |
| Name                               | RV   | Size  | Mass | Weak | Notes                                             | GP | Value |
| Heavy Leather/Gambeson/Padded      | 2    | -     | 0    | B,P  |                                                   | 2  | 1     |
| Small Shield                       | -    | -1    | 0    | -    | ⅓ Cover, +2 EV as weapon                          | 7  | 2     |

**Atlatl:** The atlatl is really just one of a multitude of devices of similar function that date back to the paleolithic that were aides to throwing javelins. (+2 Athletics to throw javelins: 2; -3 Size: 3) 7 GP.

**Axe, Hand:** Basically a hatchet or something similar. (+2 EV: 4; -3 Size: 3; Aerodynamic: 3; Additional Type: 2; Massive Damage: 3; -1 to Parry: -1) 14 GP.

**Bow, Long:** A bow is crafted for a certain pull weight (the Strength of the character at purchase), and this pull weight cannot be adjusted afterwards. When using an unfamiliar bow, the Mass is the Strength of the original owner. Due to the training needed, characters with less than 2 ranks in Athletics take a penalty. (+3 EV: 6; -1 Size: 1; Special Mass: 3; 2-Hands: -2; Not while Mounted: -2; Limited Uses, 4: -3; Requires Athletics 2, Penalty: -2) 1 GP.

**Bow, Short:** A shorter, less powerful version of the bow, able to be used while mounted or when otherwise prioritizing mobility. (+2 EV: 4; -3 Size: 3; Special Mass: 3; 2-Hands: -2; Limited Uses, 4: -3; Requires Athletics 2, Penalty: -2) 3 GP.

**Club:** Basically just a hunk of wood or bone about a meter long, used to bash your opponents brains out. (+1 EV: 2; -2 Size: 2) 4 GP.

**Club, War:** Similar to the club, the war club is just bigger, possibly with a stone or something similar attached to the end to make it hit harder. (+2 EV: 4; -1 Size: 1; +1 Mass: -1; 2 Hand: -2) 2 GP.

**Dagger:** The shortest blade weapons, originally made of stone or obsidian. Due to its balance, it gives a bonus when using it defensively. (+1 Damage: 2; -3 Size: 3; Aerodynamic: 3; Additional Type: 2; +1 to Close Combat, only for Parry: 1; -1 Reach: -1) 10 GP.

**Heavy Leather/Padded/Gambeson:** This “armor” is not much more than the pelt of an animal, a thick quilt, or even coils of rope wrapped around you, softening the blows. (+2 RV: 4; Extra Weakness: -2) 2 GP.

**Sling:** Not much more than a scrap of leather tied to a piece of rope, the sling is probably the most primitive of all ranged weapons, essentially just being a better way to throw a stone, allowing it to travel farther and hit harder. (+2 Athletics to throw stones: 2; -5 Size: 5) 7 GP.

**Shield, Small:** The basic design gives the user an amount of cover. Can also be used as a blunt weapon to bash a foe with. (Provides Cover, ⅓: 2; -1 Size: 1; +2 EV: 4) 7 GP.

**Spear:** A long pole with a sharpened point. Used almost exclusively to thrust. (+3 EV: 6; Set: 1; +1 Reach: 1; +1 Size: -1; 1.5 Hands: -1) 6 GP.

**Spear, Short/Javelin:** The javelin is not much more than a shorter spear that was designed to be throwable, but can also be used in close combat. (+2 EV: 4; -2 Size: 2; Aerodynamic: 3) 9 GP.

**Stone:** A simple rock. (+1 EV: 2; -5 Size: 5) 7 GP, although given the ubiquitous nature, should not be tracked.


## Ancient and Dark Ages Gear

Ancient pertains mostly to equipment that would have been available to Greeks, Romans, Persians, Egyptians, Celts, and the Carthaginians. The Dark Ages extend from the fall of Rome to the end of the Viking era.

|                                           Ancient and Dark Ages Gear                                               |
|----------------------------|:--:|:----:|:-----:|:----:|:----:|----------------------------------------|:--:|:-----:|
|                                                 Close Combat                                                       |
| Name                       | EV | Size | Reach | Mass | Type | Notes                                  | GP | Value |
| Short Sword                | +2 |  -2  |   0   |   0  |  P,S | +1 to Parry                            | 10 |   5   |
| Long Sword                 | +3 |  -1  |   0   |   1  |  P,S | 1.5-Hands, +2 to Parry                 | 10 |   7   |
| Light Mace                 | +2 |  -3  |   0   |   0  |   B  |                                        |  7 |   2   |
| Heavy Mace                 | +3 |  -2  |   0   |   1  |   B  | 1.5-Hands                              |  6 |   3   |
| Battle Axe                 | +3 |  -2  |   0   |   2  |  B,P | 1.5-Hands, Massive Damage, -2 to Parry |  9 |   2   |
| Two-Handed Axe             | +4 |  -1  |   0   |   2  |  B,P | 2-Hands, Massive Damage, -2 to Parry   |  9 |   3   |
|                                                 Ranged Combat                                                      |
| Name                       | EV | Size |  Cap  | Mass | Type | Notes                                  | GP | Value |
| Recurve Bow                | +4 |  -2  |   4   |  Spc |   P  | 2-Hands                                |  8 |   3   |
|                                                     Armor                                                          |
| Name                       |    |  RV  |  Size | Mass | Weak | Notes                                  | GP | Value |
| Lamellar/Laced Plate       |    |   3  |   -   |   0  |   B  |                                        |  6 |   2   |
| Hauberk/Mail/Lorica hamata |    |   4  |   -   |   2  |  B,P |                                        |  4 |   5   |
| Scale/Lorica squamata      |    |   4  |   -   |   0  |   B  |                                        |  8 |   4   |
| Laminar/Lorica segmentata  |    |   5  |   -   |   1  |   B  |                                        |  9 |   7   |
| Large Shield               |    |   -  |   -1  |   1  |   -  | ⅔ Cover, +3 EV as attack               | 10 |   2   |

**Axe, Battle:** A perfect weapon for splintering defenses with its high mass and curved cutting surface, the relatively small amount of lower quality steel used in their construction meant that they were cheap and thus easily acquired. (+3 EV: 6; -2 Size: 2; Additional Type: 3; Massive Damage: 3; +2 Mass: -2; 1.5-Hands: -1; -2 to Parry: -2) 9 GP.

**Axe, Two-Handed:** Essentially just a battle axe with a longer handle, practically speaking larged axes typically utilized larger heads as well, granting both more heft and more angular momentum. (+4 EV: 8; -1 Size: 1; Additional Type: 3; Massive Damage: 3; +2 Mass: -2; 2-Hands: -2; -2 to Parry: -2) 9 GP.

**Bow, Recurve:** Recurve bows have arms that curve away from the shooter when unstrung. This allows for a greater storage of kinetic energy resulting in more force and greater distance. (+4 EV: 8; -2 Size: 2; Spc Mass: 3; 2-Hands: -2; Reduced Uses, 4: -3) 8 GP.

**Hauberk/Mail:** Mail is one of the oldest armor techniques, reportedly created by the Celts around the early days of Rome. Because of the way it hangs from the shoulders and does not get support from the hips, it can be very tiring to wear for long. (4 RV: 8; +2 Mass: -2; Additional Weakness: -2) 4 GP.

**Lamellar/Laced Plate:** Lamellar armor is created by lacing “plates” of leather, wood, or metal to create a solid piece that is remarkably resilient to cutting and puncturing while still being very cheap to make. (3 RV: 6) 6 GP.

**Laminar:** Laminar armor is a series of horizontal overlapping metal bands laced together to form a flexible but sturdy armor. This style is best exemplified in the Lorica Segmentata of the Roman Legions, but there are many other examples in history. (5 RV: 10; +1 Mass: -1) 9 GP.

**Mace, Heavy:** With a longer handle (40 to 60 cm) and a larger, heavier head, the heavy mace was extremely effective at crushing the skulls of opponents. (+3 EV: 6; -2 Size: 2; +1 Mass: -1; 1.5-Hands: -1) 6 GP.

**Mace, Light:** The light mace typically consists of a short wooden handle (roughly 30 to 40 cm in length) topped with a heavy stone or metal ball. (+2 EV: 4; -3 Size: 3) 7 GP.

**Scale:** Scale originated in Persia as lightweight and cheap armor, being both more flexible and more resilient than chainmail. It is essentially small scales of metal sewn to a leather backing. (4 RV: 8) 8 GP.

**Shield, Large:** Like the smaller shields that were created earlier, large shields are designed to prevent attacks from actually hitting the user, as opposed to absorbing the blow. Common from hoplites and legionnaires all the way up into the vikings and knights. (Provides Cover, ⅔: 4; -1 Size: 1; +3 EV: 6; +1 Mass: -1) 10 GP.

**Short Sword:** As smelting technology grew more sophisticated it was possible to make longer, stronger blades. The Short sword (typically between 30 and 60 cm) was the weapon of the higher classes, and even into the late Roman period a sword was the most expensive weapon a person could acquire or use. (+2 EV: 4; -2 Size: 2; Additional Type: 3; +1 to Close Combat, only for Parry attempts: 1) 10 GP.

**Sword, Long:** As smelting technology advanced, the ability to create stronger and stronger alloys came within reach. These stats refer to any bladed weapon 60 to 90 cm, regardless of region of origin or other design philosophies. (+3 EV: 6; -1 Size: 1; Additional Type: 3; +2 to Close Combat, only for Parry attempts: 2; +1 Mass: -1; 1.5-Hands: -1) 10 GP.


## Medieval Gear

The time between the end of the Viking era and the advent of gunpowder and cannons. Primarily known for its increasing nationalism and the growth of the fundamentals of our modern concepts of governance, this time frame is also the period of Robin Hood and King Arthur (even though, historically speaking, if he existed it was probably during the early Dark Ages instead).

|                                        Medieval Gear                                               |
|:---------------:|:--:|:----:|:-----:|:----:|:-----:|:--------------------------------:|:--:|:-----:|
|                                         Close Combat                                               |
| Name            | EV | Size | Reach | Mass |  Type | Notes                            | GP | Value |
| Great Sword     | +4 |  -1  |   +1  |   3  |  P,S  | 2-Hands, +2 to Parry             | 10 |   7   |
| Falchion/Saber  | +2 |  -1  |   0   |   1  |   S   | +1 EV when mounted, +2 to Parry  |  8 |   5   |
| Quarterstaff    | +3 |   0  |   0   |   0  |   B   | 2-Hands, Autofire                |  7 |   1   |
| Long Spear/Pike | +4 |   2  |   +2  |   0  |   P   | 2-Hands, Set                     |  8 |   2   |
| Bardiche        | +4 |   1  |   +1  |   1  |  P,S  | 2-Hands                          |  8 |   2   |
| Glaive/Fauchard | +4 |   1  |   +1  |   1  |   S   | 2-Hands                          |  5 |   2   |
| Halberd         | +4 |   1  |   +1  |   2  | B,P,S | 2-Hands, Massive Damage          | 15 |   3   |
| War-hammer      | +5 |  -1  |   0   |   2  |  B,P  | 1.5-Hands, Massive Damage        | 15 |   3   |
| Lance           | +5 |   2  |   +2  |   1  |   P   | Requires Mount                   |  7 |   2   |
|                                        Ranged Combat                                               |
| Name            | EV | Size |  Cap  | Mass |  Type | Notes                            | GP | Value |
| Hand Crossbow   |  3 |  -3  |   1   |   0  |   P   | Long Reload (2 Turns)            |  1 |   2   |
| Crossbow        |  5 |  -2  |   1   |   0  |   P   | 1.5-Hands, Long Reload (2 Turns) |  3 |   3   |
| Arbalest        |  7 |  -2  |   1   |   1  |   P   | 2-Hands, Long Reload (2 Turns)   |  6 |   4   |
|                                           Armor                                                    |
| Name            |    |  RV  |  Size | Mass |  Weak | Notes                            | GP | Value |
| Plate           |    |   6  |   -   |   1  |   B   |                                  | 11 |   10  |

**Arbalest/Heavy Crossbow:** Building on the nature of the crossbow, the arbalest increases the force produced, leading to a much deadlier weapon. (7 EV: 14; -2 Size: 2;  Limited Uses, 1: -6; 2-Hands: -2; Slow: -2) 6 GP.

**Bardiche:** A pole-arm descended from axes, it has a much wider head than most axes, ending in a pointed end that could also be used to stab and thrust (+4 EV: 8; +1 Reach: 1; Additional Type: 3; +1 Mass: -1; 2-Hands: -2; +1 Size: -1) 8 GP. 

**Crossbow:** There is a medieval English saying that “To train a bowman it is necessary to start by training his grandfather.” While somewhat hyperbolic, it is true that archery skills take years to build to the point they are useful on the battlefield. Enter the crossbow, the world’s first point and shoot ranged weapon. (5 EV: 10; -2 Size: 2; Limited Uses, 1: -6; 1.5-Hands: -1; Slow: -2) 3 GP.

**Crossbow, Hand:** A light crossbow is smaller and less heavy, making it easily fired with one hand. This reduction in mass in addition to the lighter pull weight makes it less powerful however, limiting its usefulness. (3 EV: 6; -3 Size: 3; Limited Uses, 1: -6; Slow: -2) 1 GP.

**Saber/Shamshir/Backsword/etc:** The saber and other blades like it have curved blades, ranging from slight curves in older falchion styles to extremely curved shamshirs. While not always used from horseback,  the curved blade is supposed to aid in cutting from that vantage point. (+2 EV: 4; -1 Size: 1; +1 EV when mounted: 2; +2 to Parry: 2; +1 Mass: -1) 8 GP.

**Glaive/Fauchard/Military Scythe/Naginata/Guandao:** A group of polearms consisting of a cutting blade on a long staff. The exact constructions vary, but they all share the same concepts of a slashing weapon with considerable reach, using wide swings designed to keep foes at bay and use angular momentum to strike deadly blows. (+4 EV: 8; +1 Reach: 1; +1 Mass: -1; 2-Hands: -2; +1 Size: -1) 5 GP

**Halberd:** In many ways the ultimate polearm, the halberd combines the qualities of a spear, and axe, and a war hammer. This versatility comes at a price, however, of being less good than any one of the weapons it is composed of. (+4 EV: 8; +1 Reach: 1; 2 Additional Types: 6; Massive Damage: 3; +2 Mass: -2; 2-Hands: -2; +1 Size: -1) 15 GP.

**Lance:** The iconic weapon of the mounted knight, the lance is a highly specialized pole-arm only useful from horseback. A lance strike to a standing opponent was typically enough to kill even the most heavily armored foe. (+5 EV: 10; +2 Reach: 2; +1 Mass: -1; +2 Size: -2; Requires Mount: -2) 7 GP.

**Plate:** A huge step in armor technology, the warrior is completely encased in 1.5 to 3 millimeter thick steel designed not just to take a bashing but to also deflect blows away from vulnerable points like the head or joints. (6 RV: 12; +1 Mass: -1) 11 GP.

**Quarterstaff:** Primarily a weapon of the peasant class, the quarterstaff is basically just a long stick, although staves meant for actually fighting were often equipped with iron tips and maybe even reinforced with stips of iron along the length. (+3 EV: 6;  Autofire: 3; 2-Hands: -2) 7 GP.

**Spear, Long:** Designed for facing down charging cavalry, these spears are far too long for use in typical combat otherwise, often being 3 meters long or more. (+4 EV: 8; +2 Reach: 2; Set: 1; +1 Size: -1; 2-Hands: -2) 8 GP.

**Sword, Great:** An enormous blade, often over a meter in length, the great sword was created primarily for dealing with mounted foes. Prohibitively expensive and of dubious advantage, great swords were fairly rare on the historical battlefield. (+4 EV: 8; -1 Size: 1; +1 Reach: 1; Additional Type: 3; +2 to Parry: 2; +3 Mass: -3; 2-Hands: -2) 10 GP.

**War-hammer:** Unlike the typical image the term warhammer tends to conjure in people’s minds of a weapon resembling a sledgehammer, an actual warhammer tended to be much smaller in the head, looking more like a normal hammer on a much longer (30 to 40 cm) shaft. War hammers often had one blunt side and one spiked side. (+5 EV: 10; -1 Size: 1; Additional Type: 3; Massive Damage: 3; +2 Mass: -2) 15 GP.


## Renaissance Gear

The discovery of the new world, the frontier spirit, and the dawning (and closing) of the golden age of piracy. A revival of the ideas of the classical civilizations spurs scientific advancement, while the dominance of the firearm lays to rest the idea of body armor for a while.

|                                                                      Renaissance Gear                                                                      |
| --------------------- |:--:|:-----:|:-----:|:----:|:----:|------------------------------------------------------------------------------------|:--:|:-----:|
|                                                                        Close Combat                                                                        |
| Name                  | EV |  Size | Reach | Mass | Type | Notes                                                                              | GP | Value |
| Rapier/Small Sword    | +2 |   -1  |   0   |   0  |   P  | +2 to Parry                                                                        |  7 |   3   |
|     Ranged Combat     |    |       |       |      |      |                                                                                    |    |       |
| Name                  | EV |  Size |  Cap  | Mass | Type | Notes                                                                              | GP | Value |
| Flintlock Pistol      |  6 |   -3  |   1   |   0  |   P  | -2 Ranged Combat, Long Reload (2), Unreliable (8), Loud, 1.5-Hands                 |  1 |   2   |
| Dragon Pistol         |  7 |   -3  |   1   |   0  |   P  | Massive Damage, -2 Ranged Combat, Long Reload (2), Unreliable (8), Loud, 1.5-Hands |  2 |   2   |
| Arquebus/Musket       |  8 |   -1  |   1   |   0  |   P  | -2 Ranged Combat, Long Reload (2), Unreliable (8), Loud, 1.5-Hands                 |  1 |   3   |
| Blunderbuss/Musketoon |  9 |   -1  |   1   |   0  |   P  | Massive Damage, -2 Ranged Combat, Long Reload (2), Unreliable (8), Loud, 2-Hands   |  3 |   3   |
|                                                                     Weapon Modification                                                                    |
| Name                  | EV |  Size | Reach | Mass | Type | Notes                                                                              | GP | Value |
| Bayonet               | +1 | -4/-1 |  -1/0 |   0  |   P  | 2-Hands                                                                            |  6 |   1   |
|                                                                           Armor                                                                            |
| Name                  |    |   RV  |  Size | Mass | Weak | Notes                                                                              | GP | Value |
| Brigandine            |    |   5   |   -   |   0  |   B  | Can pass for a normal vest at a distance (-2 Penalty to be noticed)                | 12 |   3   |

**Arquebus/Musket:** A long barreled, single fire, muzzle loading firearm that was the standard for several centuries. With its longer barrel it was more accurate than the pistols of its day but for most of its lifespan it was smooth bore and thus still not as accurate as modern firearms. (8 EV: 16; -1 Size: 1; -2 Ranged Combat: -2; Slow, 2 Turns: -4; Unreliable, 8: -4, Loud: -1; 2 Hands: -2; Limited Uses, 1: -6) 1 GP

**Bayonet:** A small knife that could be attached to the end of a musket barrel to enable it to still be used in defense even when empty or when caught in close quarters. (+1 EV: 2; -4 Size: 4; -1 Reach: -1) 5 GP

**Blunderbuss/Musketoon:** A long barreled, single fire, muzzle loading firearm that was the direct ancestor of the modern shotgun. Unlike the arquebus, the blunderbuss was loaded with multiple pellets that would spread out from the funnel shaped barrel much more than a modern shotgun. (9 EV: 18; Massive Damage: 3; -1 Size: 1; -2 Ranged Combat: -2; Slow, 2 Turns: -4; Unreliable, 8: -4, Loud: -1; 2 Hands: -2; Limited Uses, 1: -6) 3 GP

**Brigandine:** Brigandine was a hybrid of plate and laminar armor, consisting of multiple large plates that overlapped but that could move freely, thus being more flexible and cheaper to create and maintain. Many times these were sewn into vests, both for comfort and to be less visible. (5 RV: 10; -2 Penalty for armor to be noticed: 2) 12 GP

**Pistol, Flintlock:** The flintlock pistol was a single fire, muzzle loading weapon designed for close quarters fights (it was highly inaccurate) and was often carried in multiples, to facilitate multiple quick firings. (6 EV: 12; -3 Size: 3; -2 Ranged Combat: -2; Slow, 2 Turns: -4; Unreliable, 8: -4, Loud: -1; 1.5 Hands: -1; Limited Uses, 1: -6) 1 GP

**Pistol, Dragon:** Much like the flintlock, the dragon was a single fire, one handed ranged weapon, but instead of a single ball, dragons were loaded with shot, making them an ancestor to modern shotguns. (7 EV: 14; Massive Damage: 3; -3 Size: 3; -2 Ranged Combat: -2; Slow, 2 Turns: -4; Unreliable, 8: -4, Loud: -1; 1.5 Hands: -1; Limited Uses, 1: -6) 2 GP

**Rapier/Small Sword:** The gentleman’s weapon, a blade made for finesse not power, thrusting not chopping, an artist’s weapon. At least that is what it’s proponent’s would tell you. A long, thin blade approximately a meter in length often with a distinctive hilt and handguard that was both decorative and defensive. (+2 EV: 4; +2 to Parry: 2; -1 Size: 1) 7 GP


## Victorian and Western Gear

The age of the British Empire is also the dawning of the industrial era, and the final era of exploration. By the end of this era the modern era begins and the entire planet is well known and science is overtaking religion as the primary way the world is understood for a vast majority of people.

|                                                             Victorian and Western Gear                                                   |
|-------------------------|:----------:|:----:|:-----:|:----:|:----:|------------------------------------------------------|:-----:|:-----:|
|                                                                    Close Combat                                                          |
| Name                    |     EV     | Size | Reach | Mass | Type | Notes                                                |   GP  | Value |
| Sword Cane              |     +2     |  -2  |   0   |  0   |   P  | -1 Hardness, -2 to be Noticed as Weapon              |   7   |   4   |
|                                                                    Ranged Combat                                                         |
| Name                    |     EV     | Size |  Cap  | Mass | Type | Notes                                                |   GP  | Value |
| Light Revolver          |      6     |  -4  |   3   |  0   |   P  | 1.5-Hands, Loud, Long Reload                         |   7   |   2   |
| Heavy Revolver          |      7     |  -3  |   3   |  0   |   P  | 1.5-Hands, Loud, Long Reload                         |   8   |   3   |
| Light Rifle             |      7     |  -1  |  1/3* |  0   |   P  | 2-Hands, Loud, Long Reload                           |  3/5  |   3   |
| Shotgun                 |      8     |  -1  |  1/3* |  0   |   P  | Massive Damage, 2-Hands, Loud, Long Reload           |  8/10 |   3   |
| Double-Barreled Shotgun |      8     |  -1  |   2   |  0   |   P  | Massive Damage, Autofire, 2-Hands, Loud, Long Reload |   12  |   4   |
|                                                                Weapon Modifications                                                      |
| Name                    |    Bonus   |      |       |      |      | Notes                                                |   GP  | Value |
| Telescopic Sight        | +2/+4/+6** |      |       |      |      | Offsets ranged penalties                             | 3/6/9 | 2/3/4 |
|           * Earlier versions had a single shot capacity, while later versions had mechanisms to feed ammo but still took time to reload.             |
| ** Earlier versions had only 1 level, so each rating is a separate item, later versions can have multiple levels and only costs the highest amount    |

**Revolver, Heavy:** Similar in design to the light revolver, the heavy revolver simply uses larger caliber, heavier bullets to provide more stopping power at the cost of size and weight. (7 EV: 14; -3 Size: 3; Limited Uses, 3: -4, Loud: -2; Slow: -2; 1.5-Hands: -1) 7 GP.

**Revolver, Light:** The first reliable weapon capable of successive firing, the revolver used a method of chambering rounds by mechanically rotating each firing chamber into place in line with the firing mechanism. (6 EV: 12; -4 Size: 4; Limited Uses, 3: -4, Loud: -2; Slow: -2; 1.5-Hands: -1) 6 GP.

**Rifle, Light:** Also referred to as a sporting rifle or a hunting rifle, this weapon is used in modern times for target shooting, hunting game, and home defense, but it was originally a military weapon. Early examples are bolt action and require manually ejecting a spent casing, while later versions take advantage of a lever to perform the action. (EV 7: 14; -1 Size: 1; Limited Uses, 1/3: -6/-4; 2 Hands: -2; Loud: -2; Slow: -2) 3/5 GP.

**Shotgun:** As the musket became the rifle, so too did the musketoon become the shotgun. Early shotguns were breech loading while later examples made use of a pump action to eject spent shells. In many ways the shotgun has been unchanged since it first appeared on the market over 150 years ago. (EV 8: 16; -1 Size: 1; Massive Damage: 3; Limited Uses, 1/3: -6/-4; 2 Hands: -2; Loud: -2; Slow: -2) 8/10 GP.

**Shotgun, Double-Barreled:** One of the first attempts at making a shotgun that could be fired repeatedly, the double barreled shotgun is still used in the modern era because it is reliable, simple, and more than sufficient for hunting or home defense. An added bonus for the design was the ability to pull the trigger on both barrels simultaneously, greatly increasing the damage potential of the weapon. (EV 8: 16; -1 Size: 1; Massive Damage: 3; Autofire: 3; Limited Uses, 2: -5; 2 Hands: -2; Loud: -2; Slow: -2) 12 GP.

**Sword Cane:** The sword cane (also called a sword-stick) is a disguised blade that masquerades as a walking cane, umbrella, or the like. This configuration enabled upper-class men and women to maintain a level of self defense while still being acceptable in regular company. (+2 EV: 4; -2 Size: 2; -2 Penalty to Notice: 2; -1 Hardness: -1) 7 GP.

**Telescopic Sight:** Also known as a scope, this optical device allows a shooter a better view of distant targets, allowing for more accurate shots over a greater distance. Early models only allowed for slight benefits, although more modern versions can greatly increase accuracy. (Telescopic, 1/2/3: 3/6/9) 3/6/9 GP.


## Modern Gear

This section deals with the weapons and equipment that has been developed in the 20th century and will likely be the standard into the near future. The vast majority of this equipment is ranged in nature although the 20th century has seen the development of several forms of effective close combat weapons as well.

|                                                                            Modern Gear                                                                   |
|--------------------------|:-----------------------:|:-------:|:-----:|:----:|:----:|-----------------------------------------------------------------------|:--:|:-----:|
|                                                                            Close Combat              |
| Name                     |            EV           |   Size  | Reach | Mass | Type | Notes                                                                 | GP | Value |
| Expandable Baton         |            +2           |    -5   |   0   |   0  |   B  |                                                                       |  9 |   2   |
| Baseball Bat             |            +2           |    -2   |   0   |   0  |   B  |                                                                       |  6 |   1   |
| Pepper Spray             |            6            |    -5   |   0   |   0  |   C  | Effect is temporary blindness and pain, chemical in nature            | 17 |   1   |
|                                                                           Ranged Combat                                |
| Name                     |            EV           |   Size  |  Cap  | Mass | Type | Notes                                                                 | GP | Value |
| Light Pistol             |            6            |    -4   |   3   |   0  |   P  | 1.5-Hands, Loud                                                       | 10 |   3   |
| Heavy Pistol             |            8            |    -3   |   3   |   1  |   P  | 1.5-Hands, Loud                                                       | 12 |   4   |
| Automatic Rifle          |            7            |    -2   |   5   |   2  |   P  | Autofire, 2-Hands, Loud                                               | 12 |   4   |
| Heavy Rifle              |            9            |    -2   |   5   |   1  |   P  | Autofire, 2-Hands, Loud                                               | 17 |   4   |
| Sniper Rifle             |            11           |    -2   |   2   |   2  |   P  | Massive Damage, 2-Hands, Loud                                         | 17 |   5   |
| Combat Shotgun           |            11           |    -2   |   3   |   2  |   P  | Massive Damage, 2-Hands, Loud                                         | 18 |   4   |
| Submachine Gun           |            4            |    -3   |   6   |   2  |   P  | Autofire, 1.5-Hands, Loud                                             |  9 |   3   |
| Light Machine Gun        |            6            |    -1   |   6   |   2  |   P  | Autofire, 2-Hands, Loud                                               | 10 |   5   |
| Heavy Machine Gun        |            8            |    -1   |   6   |   4  |   P  | Autofire, Requires Vehicle (or other mount), 2-Hands, Loud            |  9 |   5   |
| Grenade Launcher         |            -            |    -3   |   3   |   0  |   -  | 2-Hands, +5 to Athletics roll to throw grenade                        |  2 |   4   |
| Missile Launcher         |            15           |    0    |   1   |   0  |   B  | Explosive, 2-Hands, Loud, Long Reload                                 | 21 |   5   |
| Compound Bow             |            +5           |    -2   |   4   | Spec |   P  | 2-Hands                                                               | 10 |   2   |
| Taser                    |            6            |    -4   |   0   |   0  |   E  | Effect is non-lethal electrical current                               | 11 |   1   |
| Flamethrower             |            4            |    -2   |   4   |   0  |   F  | Effect is flame                                                       |  5 |   3   |
|                                                                        Weapon Modifications         |
| Name                     |         Bonuses         |         |       |      |      | Notes                                                                 | GP | Value |
| Bipod/Tripod             |         -2 Mass         |         |       |      |      |                                                                       |  2 |   1   |
| Concealable Holster      |  -2 Perception to Spot  |         |       |      |      | Only for weapons of size -3 and less                                  |  2 |   1   |
| Laser Sight/Reflex Sight |     +1 Ranged Combat    |         |       |      |      |                                                                       |  1 |   2   |
| Night Scope              | +2/+4 Attack, +4 Percep |         |       |      |      | Offsets ranged penalties, Offsets darkness penalties                  | 10 |   3   |
| Silencer                 |                         |         |       |      |      | Reduces sound produced by firearms                                    |  2 |   2   |
| Armor Piercing Ammo      |      Massive Damage     |         |       |      |      |                                                                       |  3 |   1   |
| Subsonic Ammo            |                         |         |       |      |      | Reduces sound produced by firearms                                    |  2 |   1   |
|                                                                            Explosives              |
| Name                     |            EV           |   Size  |  Cap  | Mass | Type | Notes                                                                 | GP | Value |
| Frag Grenades            |            8            |    -5   |   2   |   0  |   B  | Aerodynamic, Explosive, Loud                                          | 21 |   2   |
| Flashbang Grenades       |            8            |    -5   |   2   |   0  |   V  | Aerodynamic, Explosive, Loud, Effect is temporary blindness, deafness | 21 |   2   |
| Gas Grenades             |            8            |    -5   |   2   |   0  |   C  | Aerodynamic, Explosive, Loud, Effect is temporary blindness, pain     | 21 |   2   |
|                                                                             Armor                  |
| Name                     |                         |    RV   |  Size | Mass | Weak | Notes                                                                 | GP | Value |
| Armored Vest             |                         |    3    |   -   |   0  |   B  | Hardened, -1 to be noticed under clothing                             | 10 |   3   |
| Light Ballistic Armor    |                         |    4    |   -   |   0  |   B  | Hardened                                                              | 11 |   4   |
| Heavy Ballistic Armor    |                         |    6    |   -   |   0  |   B  | Hardened                                                              | 15 |   5   |
| Riot Shield              |                         |    -    |   -1  |   1  |   -  | Full Cover, +3 EV to bash                                             | 12 |   2   |
|                                                                       Armor Modifications                                                                               |
| Name                     |                         | Bonuses |       |      |      | Notes                                                                 | GP | Value |
| Ballistic Reinforcement  |                         |  +3 RV  |       |      |      | One time protection                                                   |  3 |   2   |
| Chemical Seal            |                         |   6 RV  |       |      |      | Only protects against Chemical (C) damage                             |  3 |   1   |
| Flame Retardant Coating  |                         |   6 RV  |       |      |      | Only protects against Flame/Fire (F) damage                           |  3 |   1   |
| Insulated Lining         |                         |   6 RV  |       |      |      | Only protects against Cold damage                                     |  3 |   1   |
| Non-Conductive Coating   |                         |   6 RV  |       |      |      | Only protects against Electric (E) damage                             |  3 |   1   |

**Ammo, Armor Piercing:** When loaded into a firearm, this ammo gives the weapon the Piercing aspect. When purchased with CP, this represents a single magazine of the ammo, which is replenished each session (or between adventures, as the GM sees fit). Every weapon is understood as coming with a magazine of standard ammo for free. (Massive Damage: 3) 3 GP.

**Ammo, Subsonic:** This ammo travels more slowly than standard ammunition. This avoids breaking the speed of sound, making it considerably quieter. This either removes the Loud quality, or if used with a silencer will give the weapon the Silent quality. (Silent, if not Loud, if Loud, then just removes Loud: 2) 2 GP.

**Armored Vest:** In the early parts of the modern era this would be a flak jacket, although these have been superseded by modern kevlar vests. More bullet-resistant than bullet-proof, these armored vests are still far superior to no armor at all. (3 RV: 6; Hardened: 3; -1 to Perception to be noticed when under clothing: 1) 10 GP.

**Ballistic Armor, Heavy:** The heaviest, most protective armor available, using state of the art armor technology. Besides top secret government projects, this is the top of the line in modern protection. (6 RV: 12; Hardened: 3) 15 GP.

**Ballistic Armor, Light:** The classic “SWAT Armor” or “riot gear,” it is made of multiple layers of kevlar plates. (4 RV: 8; Hardened: 3) 11 GP.

**Ballistic Reinforcement:** The ballistic reinforcement is essentially an extra plate that straps across the chest of other armor that acts as an additional layer of protection. This plate is destroyed after taking a couple of shots. (+3 RV: 6; Ablative: 3) 3 GP.

**Baseball Bat/Cricket Bat/Hockey Stick:** While not designed for striking humans directly, many types of modern sports equipment are effective when deployed as weapons. For all intents and purposes treat all similar types of gear as the same. (+2 EV: 4; -2 Size: 2) 6 GP.

**Baton. Expandable:** Designed for law enforcement agents, this weapon resembles a 20 cm cylinder with a small switch on the side. When activated a spring mechanism extends the collapsible baton to its full 60 cm length and locks it into place at which point it is a formidable non-lethal weapon. Then with a twist the entire thing collapses back again. (+2 EV: 4; -5 Size: 5) 9 GP.

**Bi-pod/Tripod:** A bipod is a simple add-on that, when deployed, creates a stable point for firing bulky weapons or those with high recoil. Typically these are deployed in a prone position, but can also be used on walls, rocks, or other objects from a crouched or even standing position. (-2 Mass: 2) 2 GP.

**Bow, Compound:** Sharing the benefits of the traditional bow with the added edge of modern materials science and precise engineering, the compound bow has more power and a lighter pull weight. (+5 EV: 10; -2 Size: 2; Special Mass: 3; Limited Uses, 4: -3; 2 Hands: -2) 10 GP.

**Chemical Seal:** A non-reactive coating along with airtight seals at each joint protects the wearer from airborne and liquid toxins and poisons she may come into contact with. (6 RV: 12; Multiple Weaknesses, 3: 9; Effect protected against is chemicals) 3 GP.

**Concealable Holster/Sheath:** A concealable holster or sheath is designed to be out of sight. Even the best options in concealment can only hide small weapons, and even then only from a distance. A pat down or other method is practically guaranteed to find the hidden weapon. (-2 Penalty when trying to spot a weapon, Only for weapons Size -3 or less: 2) 2 GP.

**Flame Retardant Coating:** Covered in reflective materials that reflect heat and filled with not convective materials, this is similar to what firemen and stuntmen might use. (Immunity 3: Fire) 9 CP.

**Flamethrower:** One of the most terrifying modern weapons, the flamethrower ejects a stream of flammable liquid across a flame producing a line of blazing fire. Used in WWII against entrenched Japanese forces on Pacific islands, it proved brutally effective. (6 RV: 12; Multiple Weaknesses, 3: 9; Effect protected against is fire and heat) 3 GP.

**Grenade Launcher:** Despite the name, the grenade launcher does not, in fact, launch what the average person thinks of as a grenade (although the grenades below will give the correct stats). Instead of a hand grenade the launcher lobs explosive charges at a low velocity on a short ballistic arc. (+5 Athletics to throw grenades: 5; -3 Size: 3; Limited Uses, 3: -4; 2 Hands: -2) 2 GP.

**Grenade, Flash:** Instead of killing targets, these are designed to emit an extremely bright light that blinds and incapacitates targets. (8 EV: 16; Explosive: 3; Aerodynamic: 3; -5 Size: 5; Limited Uses, 2: -5; Effect is blinding flash) 21 GP.

**Grenade, Frag:** Standard fragmentary grenades. When they explode they launch tiny bits of shrapnel in all directions at high speed. (8 EV: 16; Explosive: 3; Aerodynamic: 3; -5 Size: 5; Limited Uses, 2: -5; Loud: -1) 21 GP.

**Grenade, Gas:** A different method of non-lethal combat, this emits tear gas, an extremely unpleasant chemical that burns the eyes, nose and throat. (8 EV: 16; Explosive: 3; Aerodynamic: 3; -5 Size: 5; Limited Uses, 2: -5; Effect is temporary blindness and pain) 21 GP.

**Insulated Lining:** Warm fur or the like for the far north and lightweight materials for protection from the high desert sun. (6 RV: 12; Multiple Weaknesses, 3: 9; Effect protected against is extreme cold) 3 GP.

**Laser Sight/Reflex Sight:** Two different technologies that ultimately do the same job, which is to allow for the rapid acquisition of targets. These types of sights do not make it easier to hit distant targets (in fact, because light travels in straight lines while bullets do not, they may make long distance firing less accurate) they do help to quickly establish whether the target is in the line of fire. (+1 Ranged Attack: 1) 1 GP.

**Machine Gun, Heavy:** Unlike the light machine gun, the medium and heavy machine guns are designed to be stationary, either mounted to a vehicle or from a bi-pod or tripod on the ground. (8 EV: 16; -1 Size: 1; Autofire: 3; +4 Mass: -4; Requires Vehicle: -3; Limited Uses, 6: -1; Loud: -1; 2 Hands: -2) 9 GP.

**Machine Gun, Light:** Designed primarily as an easily portable suppression weapon, the original light machine guns happened to be light enough that a soldier could fire the weapon while standing and moving, allowing him more mobility than heavier weapons. (6 EV: 12; -1 Size: 1; Autofire: 3; +2 Mass: -2; Limited Uses, 6: -1; Loud: -1; 2 Hands: -2) 10 GP.

**Missile Launcher/RPG Launcher:** The largest, most destructive hand-held weapon in existence, the missile launcher was created as an effective anti-tank and sometimes anti-air weapon. (15 EV: 30; Explosive: 3; Limited Uses, 1: -6; Slow: -3; 2 Hands: -2, Loud: -1) 21 GP.

**Night Vision Scope:** In addition to allowing greater accuracy at longer distances, this scope also compensates for some level of darkness. (Telescopic, 2: 6; +4 Perception to counter darkness penalties: 4) 10 GP .

**Non-Conductive Coating:** Rubberized coatings ward off casual shocks, while built-in conductive channels leading to grounding connections in the boots channel excess energy safely away from the body. (6 RV: 12; Multiple Weaknesses, 3: 9; Effect protected against is electricity) 3 GP.

**Pepper Spray:** Essentially personalized chemical warfare, pepper spray is a non-lethal aerosol delivery system that fills the air (or the assailant’s face) with acrid liquid which burns to inhale and temporarily blinds if it gets in the eyes. (6 EV: 12; -5 Size: 5; Effect is temporary blindness and pain) 17 GP.

**Pistol, Heavy:** A larger handgun chambered for heavier ammunition. The higher mass means more inertia which in turn means higher penetration and stopping power. (8 EV: 16; -3 Size: 3; +1 Mass: -1; Limited Uses, 3: -4; Loud: -1; 1.5 Hands: -1) 13 GP.

**Pistol, Light:** A smaller handgun designed more for concealability than stopping power. (6 EV: 12; -4 Size: 4; Limited Uses, 3: -4; Loud: -1; 1.5 Hands: -1) 10 GP.

**Rifle, Automatic:** Based on the idea that a higher number of less powerful bullets is preferable to a single high power round, automatic rifles are accurate into medium distances but able to lay down fairly continuous fire for suppression as well, making them extremely versatile. (7 EV: 14; -2 Size: 2; Autofire: 3; +2 Mass: -2; Limited Uses, 5: -2; Loud: -1; 2 Hands: -2) 12 GP.

**Rifle, Heavy/Battle Rifle:** The other side of the coin from the automatic rifle, the battle rifle operates best in situations where long ranges and short controlled bursts are required, sacrificing the number of rounds sent down range for higher stopping power and improved distance. (9 EV: 18; -2 Size: 2; Autofire: 3; +1 Mass: -1; Limited Uses, 5: -2; Loud: -1; 2 Hands: -2) 17 GP.

**Rifle, Sniper:** With an elongated barrel, and extremely high caliber, high energy ammo, the sniper rifle is the ultimate in long range death dealing, capable of making a kill at ranges in excess of a kilometer. (11 EV: 22; Massive Damage: 3; -2 Size: 2; +2 Mass: -2; Limited Uses, 2: -5; Loud: -1; 2 Hands: -2) 17 GP.

**Shield, Riot:** A large yet lightweight shield made of steel, plexiglass, and advanced plastics, the riot shield is the go to weapon when things get rowdy. The see-through design makes it possible to defend from more attacks while remaining covered, and the large size makes it easy to take up formation and keep large groups at bay safely. (Provides Cover, Full: 6; -1 Size: 1; +3 EV: 6; +1 Mass: -1) 12 GP.

**Shotgun, Combat:** The go to weapon for breaching doors and mowing down enemies in close quarters. What it lacks in range it makes up for in pure stopping power. (11 EV: 22; Massive Damage: 3; -2 Size: 2; +2 Mass: 2; Limited Uses, 3: -4; Loud: -1; 2 Hands: -2) 18 GP.

**Silence/Suppressor:** It’s not really silent. There. The secret is out. That said, a suppressor in conjunction with subsonic ammo is dramatically quieter (at least with certain weapons/calibers) than a standard weapon. (Silent, if not Loud, if Loud, then just removes Loud: 2) 2 GP.

**Submachine Gun:** The submachine gun is light, small, and capable of laying down large amounts of ammo in a short period of time. The weapon is not known for stopping power or accuracy (due to the recoil with such a light frame) and were largely replaced by automatic rifles in modern militaries. (4 EV: 8; -3 Size: 3; Autofire: 3; +2 Mass: -2: Limited Uses, 6: -1; Loud: -1; 1.5 Hands: -1) 9 GP.

**Taser:** A non-lethal method of taking out the bad guys, a taser fires a pair of darts that stick into the target, allowing electricity to arc across them knocking the target unconscious. It is also capable of being used in close combat without a dart loaded. (6 EV: 12; -4 Size: 4; Limited Uses, 2: -5; Effect is Non-Lethal) 11 GP.


## Near Future Gear

No matter how dedicated to peaceful negotiations your character is, rarely do runs in a world dominated by power hungry mega-corp execs and twitchy private security forces go smoothly. The following weapons, armor, and other gear will keep your character on the right side of the ground.

|                                                                      Near Future Gear                                                                             |
|-----------------------|:----------------:|:----:|:-----:|:----:|:----:|------------------------------------------------------------------------------|:--:|:-----:|
|                                                                        Close Combat                                                                               |
| Name                  |        EV        | Size | Reach | Mass | Type | Notes                                                                        | GP | Value |
| Ceramic Knife         |        +2        |  -3  |   -1  |   0  |   P  | Aerodynamic                                                                  |  9 |       |
| Forearm Snap Blades   |        +2        |  -4  |   0   |   0  |   P  | +2 to Parry                                                                  | 10 |       |
| Shock Gloves          |         3        |  -5  |   -2  |   0  |   E  | Capacity 4, Effect is non-lethal electricity                                 |  6 |       |
| Shock Baton           |         5        |  -3  |   0   |   0  |   E  | Capacity 3, Effect is non-lethal electricity                                 |  9 |       |
| Shock Staff           |         5        |  -1  |   0   |   0  |   E  | Capacity 4, Autofire, Effect is non-lethal electricity                       | 12 |       |
| Tech-Knife            |        +3        |  -3  |   -1  |   0  |  S,? | +1 to Parry, Secondary EV type can be switched between electricity and heat  | 12 |       |
| Tech-Katana           |        +4        |  -2  |   0   |   0  |  S,? | 1.5-Hands, +1 to Parry, Secondary EV type can be electricity or heat         | 14 |       |
|                                                                     Weapon Modification                                                                           |
| Name                  |     Benefits     |      |       |      |      | Notes                                                                        | GP | Value |
| WEAPONlink            |      Special     |      |       |      |      | Can fire weapon remotely, get updates on condition, other bonuses            |  3 |       |
| WEAPONsight           | +1 Ranged Combat |      |       |      |      |                                                                              |  1 |       |
| Stabilization Harness |      -2 Mass     |      |       |      |      | Can also use weapons that “require vehicle”                                  |  2 |       |
|                                                                            Armor                                                                                  |
| Name                  |                  |  RV  |  Size | Mass | Weak | Notes                                                                        | GP | Value |
| Armored Business Suit |                  |   4  |   -   |   -  |   B  | -3 to Perception to notice it is armored                                     | 11 |       |
| Infiltration Suit     |                  |   6  |   -   |   -  |   B  | +2 to Stealth rolls                                                          | 14 |       |
| Nano Armor            |                  |   8  |   -   |   -  |   -  | +2 to Strength, +2 to Athletics, No Weakness                                 | 24 |       |

**Baton, Shock:** Another weapon designed primarily for security forces, it delivers a powerful electric shock that can render foes unconscious. Roughly 60 cm long with a non-conductive handle, when activated it arcs with blue electricity. When not activated, treat it as an expandable baton, although it is not collapsible. (5 EV: 10; -3 Size: 3; Limited Uses, 3: -4; Effect is non-lethal electricity) 9 GP.

**Discreet Armored Business Suit:** What looks on the surface like a well tailored suit is, in fact, a high tech armor suit that utilizes small, discreet pockets that hide non-newtonian fluid armor over all the most sensitive parts of the body. This piecemeal design is not going to keep you from harm, but it might keep you from a slab in the morgue. (4 RV: 8; -3 Perception to determine it is armor: 3) 11 GP.

**Forearm Snap Blades:** Favored by assassins everywhere, these unassuming weapons are sheathed along the forearm. With a button press or pre-programmed gesture the blades spring into place, complete with a handle to improve leverage and applicable force. The blades themselves are about 30 cm long. (+2 EV: 4; -4 Size: 4; +2 to Parry attempts: 2) 10 GP.

**Infiltration Suit:** The infiltration suit is a thin mesh not dissimilar from ancient chainmail except that it’s made of carbon nano-tubes weaved together to give amazing durability in a light, flexible package. In addition to its protective nature, this armor is capable of producing an energy field that bends the ambient light around the user, granting them a bonus to Stealth. (6 RV: 12; +2 Stealth: 2 GP) 14 GP.

**Knife, Ceramic:** A short, thin blade made out of exotic ceramics. Very tough, non-conductive, undetectable by most weapons scanners, and extremely good at keeping an edge, these knives are on the cutting edge of materials technology (pun intended). Their one drawback is that, while resilient to striking head-on or along the blade, they do not handle twisting or shearing forces well, tending to break instead of bend. (+2 EV: 4; -3 Size: 3; Aerodynamic: 3; -1 Reach: -1) 9 GP.

**Nano Armor:** The pinnacle of armor technology, Nano Armor is constructed of diamond hard carbon nano-tubes that are reinforced by pockets of non-newtonian fluid, ceramic “dragon scale,” and kevlar, all placed over a hydraulic carbon-fiber and titanium skeleton that grants the soldier wearing it extreme strength and speed as well. (8 RV: 16; +2 Strength: 4; +2 Athletics: 2; No Weakness: 2) 24 GP.

**Shock Gloves:** Designed more for concealability than effectiveness, these lightweight gloves contain contact points which, when activated, send a painful electric jolt into the opponent, possibly knocking them unconscious. These gloves recharge themselves through the movement of the character, never needing to be recharged. (3 EV: 6; -5 Size: 5; Limited Uses, 4: -3; -2 Reach: -2; Effect is non-lethal electricity) 6 GP.

**Shock Staff:** A larger version of the shock batons, these weapons have arcing electricity on both ends than can render foes unconscious. Capable of delivering a higher charge, a shock staff is capable of taking down a foe regardless of how large or tough they may be. (5 EV: 10; -1 Size: 1; Autofire: 3; Limited Uses, 4: -2; Effect is non-lethal electricity) 12 GP.

**Stabilization Harness:** The stabilization harness is essentially a mechanized assist to allow normal humans to use large, heavy weapons more effectively, such as heavy machine guns and the like. It consists of a pair of articulated hydraulic arms that help hold the mass as well as stabilize the recoil. One arm attaches under the one arm, and the other attaches over the opposite shoulder (adjustable for lefties) to the backpack power device that powers both the harness and the heavy weapon in question (if required). (-2 Mass: 2) 2 GP.

**Tech-Katana:** Quite possibly the most technologically advanced weapon in the world, a tech-katana is a lightweight combination of super durable carbon nanotube technology, exotic plastics designed for aerospace use, and traditional alloys that never needs sharpening, is capable of cutting through any mundane material, and can withstand forces that would break a diamond. They are also capable of being modded to deliver intense electrical current, glow in several different colors bright enough to act as a light source, and be connected to a comm to upload physics data in real time and do other interesting tricks. (+4 EV: 8; -2 Size: 2; Additional Type: 3; +2 to Parry attempts: 2; 1.5 Hands: -1) 14 GP.

**Tech-Knife:** Much like its larger cousin, the tech-knife is a formidable weapon that is far outside the reach of any but the richest mercenary. It does everything that the tech-katana does, but is smaller and more concealable. (+3 EV: 6; -3 Size: 3; Additional Type: 3; +1 to Parry attempts: 1; -1 Size: -1) 12 GP.

**WEAPONlink:** The Wireless Environment Aware Projectile Operation Network link (WEAPONlink for short) is a modification that allows information from a weapon to be passed to a computer, analyzed, and then passed to the user of the weapon for up to the second details about the weapon’s performance, such as heat build-up, remaining ammo, fire modes, wear and tear on moving parts and more. This also allows a user to change fire modes, eject clips, turn safeties on or off and even fire the weapon without actually touching it. (Special Effect: 3) 3 GP.

**WEAPONsight:** Additional functionality for the WEAPONlink system, WEAPONsight allows the user to get video feed from the weapon’s sights through the link, allowing the weapon to be accurately fired around corners or from the hip with no penalties. (+1 Ranged Combat: 1) 1 GP.


## Space Opera Gear

The following equipment would not be out of place in any sci-fi setting in the distant future, and utilizes tech that is of questionable plausibility, but this isn’t meant to be a physics lesson.

|                                                       Space Opera Gear                                                      |
|------------------------|:--:|:----:|:-----:|:----:|:----:|-----------------------------------------------------|:--:|:-----:|
|                                                         Close Combat                                                        |
| Name                   | EV | Size | Reach | Mass | Type | Notes                                               | GP | Value |
| Plasma Knife           |  9 |  -4  |   -1  |   -  |   F  | Massive Damage, Effect is plasma burns              | 24 |       |
| Plasma Sword           |  9 |  -4  |   0   |   -  |   F  | Massive Damage, 1.5-Hands, Effect is plasma burns   | 24 |       |
| Plasma Lance           |  9 |  -1  |   1   |   -  |  B,F | Massive Damage, 2-Hands, Effect is plasma burns     | 24 |       |
|                                                          Ranged Combat                                                      |
| Name                   | EV | Size |  Cap  | Mass | Type | Notes                                               | GP | Value |
| Plasma Pistol          |  8 |  -4  |   4   |   0  |   F  | Massive Damage, 1.5-Hands, Effect is plasma burns   | 19 |       |
| Plasma Rifle           | 10 |  -2  |   6   |   0  |   F  | Massive Damage, Autofire, 2-Hands, Effect is plasma | 25 |       |
| Mass Driver Rifle      | 12 |  -2  |   2   |   0  |   P  | Massive Damage, 2-Hands                             | 22 |       |
|                                                              Armor                                                          |
| Name                   |    |  RV  |  Size | Mass | Weak | Notes                                               | GP | Value |
| Personal Energy Shield |    |  10  |   -   |   -  |   B  | Hardened, Ablative, Capacity 3                      | 16 |       |

**Knife, Plasma:** By harnessing the extreme energies involved in the “4th phase of matter” called plasma within a shaped electromagnetic field, a plasma knife is capable of cutting through nearly anything with ease. Using different gases to create the blade gives it a different color, but has little to no effect on the actual usefulness of said blade. When turned off, the weapon is a small unassuming rod approximately 20 cm in length. (9 EV: 18; -4 Size: 4; Massive Damage: 3; -1 Reach: -1) 24 GP

**Lance, Plasma:** Essentially a plasma polearm, its length is often used as a quarterstaff for non-lethal encounters, while the cutting edge can be ignited for an extra edge. (9 EV: 18; -1 Size: 1; Massive Damage: 3; Additional Type: 3; +1 Reach: 1; 2 Hands: -2) 24 GP.

**Personal Energy Shield:** Using the same technology that powers the plasma weapons and the mass drivers, soldiers of the distant future can rely more on armor in the form of a personal force field that slows and repulses plasma. The problem is that these shields take a huge amount of energy to power, and after a few hits they can be rendered inoperable for a short time. (10 RV: 20; Hardened: 3; Ablative: -3; Limited Uses, 3: -4) 16 GP.

**Pistol, Plasma:** A small, pocket sized fusion generator produces plasma which is encapsulated in an electromagnetic field and launched via mass driver at high velocity. This plasma burns through nearly any normal material within microseconds. (8 EV: 16; -4 Size: 4; Massive Damage: 3; Limited Uses, 4: -3; 1.5 Hands: -1) 19 GP.

**Rifle, Mass Driver:** The largest, most powerful handheld mass driver possible, it’s not restricted to just plasma, but is utilized to launch solid slugs of tungsten at relativistic speeds through nearly anything. By far the most devastating handheld weapon ever devised. Because of the nearly instantaneous travel time of the slug there is little concern for wind, gravity, or any normal factors when aiming. (12 EV: 24; -2 Size: 2; Massive Damage: 3; Limited Uses, 2: -5; 2 Hands: -2) 22 GP

**Rifle, Plasma:** Similar to the plasma pistol, the rifle can create even larger amounts of plasma, doing more damage, or even firing volleys of shots in rapid succession. (10 EV: 20; -2 Size: 2; Massive Damage: 3; Autofire: 3; Limited Uses, 6: -1; 2 Hands: -2) 25 GP.

**Sword, Plasma:** Like the plasma knife, just more powerful and thus capable of creating a longer blade. (9 EV: 18; -4 Size: 4; Massive Damage: 3; 1.5 Hands: -1) 24 GP


# Appendix C: Example NPCs
