# VERS Roleplaying

Welcome to VERS (pronounced like the <em>verse</em> of a poem or song), the open source, 3d6 based generic system. VERS stands for Variable Environment Roleplay System, and is an attempt to take the huge world of RPG mechanics out there and distill them into a system that uses just 3d6 for every dice roll, no exceptions. Secondarily, combat is not just physical, but mental and social, with characters not just having to watch their physical health but also their focus and morale to be successful. Finally, interactions are super fast, with all rolls made by the players (excepting, of course, the occasional secret roll by the GM) and a stunt system that makes each roll unique and tense and not just the typical "I swing at him" cadence.

So if you are in the mood to try something different, please give it a shot. The core rules can be accessed either here in the [online rules reference](https://versrpg.org/online-rules-reference/), or download a copy of the most recently compiled PDF.


## Open Source, free to use, free to change

VERS is an open game. This is not necessarily a new concept, as the OGL has been around for close to 20 years now, and games like FUDGE were doing it even longer. What is different about VERS is that if you have changes or ideas you can submit them. Not everything will get merged into the main text, but over the course of time VERS stands to be the most balanced and versatile RPG system out there through the work of the entire community. So take a look at the rules and feel free to submit new rules, revisions, corrections, or anything else that you come up with. Just note that all submissions are also considered open.

Powered by [Creative Commons CC-BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/).

![Creative Commons](https://versrpg.org/wp-content/uploads/2019/01/cc.png) ![Attribution](https://versrpg.org/wp-content/uploads/2019/01/by.png) ![Share Alike](https://versrpg.org/wp-content/uploads/2019/01/sa.png)
